package common

import (
	"math/big"
	"testing"
)

func TestIntAry_Equals_01(t *testing.T) {
	nStr1 := "000549721.32178000"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()
	ia2.CopyIn(&ia, false)

	if !ia.Equals(&ia2) {
		t.Error("Error: ia NOT EQUAL to ia2!")
	}

}

func TestIntAry_Equals_02(t *testing.T) {
	nStr1 := "-000549721.32178000"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.CopyToBackUp()

	ia2 := IntAry{}.New()
	ia2.CopyIn(&ia, true)

	if !ia.Equals(&ia2) {
		t.Error("Error: ia NOT EQUAL to ia2!")
	}

	if !ia.BackUp.Equals(&ia.BackUp) {
		t.Error("Error: ia.Backup != ia2.Backup!")
	}

}

func TestIntAry_Equals_03(t *testing.T) {
	nStr1 := "-000549721.32178000"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.CopyToBackUp()

	ia2 := IntAry{}.New()
	ia2.CopyIn(&ia, true)

	ia2.SignVal = 1

	if ia.Equals(&ia2) {
		t.Error("Error: ia EQUALS ia2!")
	}

}

func TestIntAry_Equals_04(t *testing.T) {
	nStr1 := "-000549721.32178000"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.CopyToBackUp()

	ia2 := IntAry{}.New()
	ia2.CopyIn(&ia, true)

	ia2.BackUp.SignVal = 1

	if !ia.Equals(&ia2) {
		t.Error("Error: ia NOT EQUAL ia2!")
	}

	if ia.BackUp.Equals(&ia2.BackUp) {
		t.Error("Error: ia.BackUp SHOULD NOT EQUAL ia2.BackUp")
	}

}

func TestIntAry_Floor_01(t *testing.T) {
	nStr1 := "99.925"
	expected := "99.000"
	precision := 3

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Floor()

	if err != nil {
		t.Errorf("Received Error from ia.Floor(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Floor_02(t *testing.T) {
	nStr1 := "-99.925"
	expected := "-100.000"
	precision := 3

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Floor()

	if err != nil {
		t.Errorf("Received Error from ia.Floor(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Floor_03(t *testing.T) {
	nStr1 := "0.925"
	expected := "0.000"
	precision := 3

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Floor()

	if err != nil {
		t.Errorf("Received Error from ia.Floor(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Floor_04(t *testing.T) {
	nStr1 := "2.0"
	expected := "2.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Floor()

	if err != nil {
		t.Errorf("Received Error from ia.Floor(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Floor_05(t *testing.T) {
	nStr1 := "-2.7"
	expected := "-3.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Floor()

	if err != nil {
		t.Errorf("Received Error from ia.Floor(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Floor_06(t *testing.T) {
	nStr1 := "-2"
	expected := "-2"
	precision := 0

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Floor()

	if err != nil {
		t.Errorf("Received Error from ia.Floor(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Floor_07(t *testing.T) {
	nStr1 := "2.9"
	expected := "2.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Floor()

	if err != nil {
		t.Errorf("Received Error from ia.Floor(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_GetInt_01(t *testing.T) {

	nStr1 := "50"
	expected := int(50)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	result, err := ia.GetInt()

	if err != nil {
		t.Errorf("Error returned from ia.GetInt(). nStr= '%v' .  Error= %v", nStr1, err)
	}

	if expected != result {
		t.Errorf("Error Expected ia.GetInt()= %v .  Instead ia.GetInt()= %v .", expected, result)
	}

}

func TestIntAry_GetInt_02(t *testing.T) {

	nStr1 := "-50"
	expected := int(-50)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	result, err := ia.GetInt()

	if err != nil {
		t.Errorf("Error returned from ia.GetInt(). nStr= '%v' .  Error= %v", nStr1, err)
	}

	if expected != result {
		t.Errorf("Error Expected ia.GetInt()= %v .  Instead ia.GetInt()= %v .", expected, result)
	}

}

func TestIntAry_GetInt_03(t *testing.T) {

	nStr1 := "2147483647"
	expected := int(2147483647)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	result, err := ia.GetInt()

	if err != nil {
		t.Errorf("Error returned from ia.GetInt(). nStr= '%v' .  Error= %v", nStr1, err)
	}

	if expected != result {
		t.Errorf("Error Expected ia.GetInt()= %v .  Instead ia.GetInt()= %v .", expected, result)
	}

}

func TestIntAry_GetInt_04(t *testing.T) {

	nStr1 := "2147483648"
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	_, err := ia.GetInt()

	if err == nil {
		t.Error("Input Value Exceeded maximum allowable value for a 32-bit Int. This should have thrown an error. However, no error was thrown! ")
	}

}

func TestIntAry_GetInt_05(t *testing.T) {

	nStr1 := "-2147483649"
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	_, err := ia.GetInt()

	if err == nil {
		t.Error("Input Value was less than the minimum allowable value for a 32-big Int. This should have thrown an error. However, no error was thrown! ")
	}

}

func TestIntAry_GetInt64_01(t *testing.T) {

	nStr1 := "50"
	expected := int64(50)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	result, err := ia.GetInt64()

	if err != nil {
		t.Errorf("Error returned from ia.GetInt64(). nStr= '%v' .  Error= %v", nStr1, err)
	}

	if expected != result {
		t.Errorf("Error Expected ia.GetInt64()= %v .  Instead ia.GetInt64()= %v .", expected, result)
	}

}

func TestIntAry_GetInt64_02(t *testing.T) {

	nStr1 := "-50"
	expected := int64(-50)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	result, err := ia.GetInt64()

	if err != nil {
		t.Errorf("Error returned from ia.GetInt64(). nStr= '%v' .  Error= %v", nStr1, err)
	}

	if expected != result {
		t.Errorf("Error Expected ia.GetInt64()= %v .  Instead ia.GetInt64()= %v .", expected, result)
	}

}

func TestIntAry_GetInt64_03(t *testing.T) {

	nStr1 := "9223372036854775807"
	expected := int64(9223372036854775807)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	result, err := ia.GetInt64()

	if err != nil {
		t.Errorf("Error returned from ia.GetInt64(). nStr= '%v' .  Error= %v", nStr1, err)
	}

	if expected != result {
		t.Errorf("Error Expected ia.GetInt64()= %v .  Instead ia.GetInt64()= %v .", expected, result)
	}

}

func TestIntAry_GetInt64_04(t *testing.T) {

	nStr1 := "9223372036854775808"
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	_, err := ia.GetInt64()

	if err == nil {
		t.Error("Input Value Exceeded maximum allowable value for an Int64. This should have thrown an error. However, no error was thrown! ")
	}

}

func TestIntAry_GetInt64_05(t *testing.T) {

	nStr1 := "-9223372036854775809"
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	_, err := ia.GetInt64()

	if err == nil {
		t.Error("Input Value was less than the minimum allowable value for an Int64. This should have thrown an error. However, no error was thrown! ")
	}
}

func TestIntAry_GetInt64_06(t *testing.T) {

	nStr1 := "-9223372036854775808"
	expected := int64(-9223372036854775808)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	result, err := ia.GetInt64()

	if err != nil {
		t.Errorf("Error returned from ia.GetInt64(). nStr= '%v' .  Error= %v", nStr1, err)
	}

	if expected != result {
		t.Errorf("Error Expected ia.GetInt64()= %v .  Instead ia.GetInt64()= %v .", expected, result)
	}

}

func TestIntAry_GetFractionalDigits_01(t *testing.T) {

	nStr1 := "2.7894"
	expected := "0.7894"
	precision := 4
	signVal := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetFractionalDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if false != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'\n", false, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'\n", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetFractionalDigits_02(t *testing.T) {

	nStr1 := "2"
	expected := "0"
	precision := 0
	signVal := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetFractionalDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if true != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'\n", true, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'\n", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetFractionalDigits_03(t *testing.T) {

	nStr1 := "2.00"
	expected := "0.00"
	precision := 2
	signVal := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetFractionalDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if true != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'\n", true, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'\n", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetFractionalDigits_04(t *testing.T) {

	nStr1 := "-2.978562154907"
	expected := "0.978562154907"
	precision := 12
	signVal := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetFractionalDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if false != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'\n", false, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'\n", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetIntegerDigits_01(t *testing.T) {

	nStr1 := "997562.4692"
	expected := "997562"
	precision := 0
	signVal := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetIntegerDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if false != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'\n", false, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'\n", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetIntegerDigits_02(t *testing.T) {

	nStr1 := "0.4692"
	expected := "0"
	precision := 0
	signVal := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetIntegerDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if true != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'\n", true, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'\n", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetIntegerDigits_03(t *testing.T) {

	nStr1 := "-987.4692"
	expected := "-987"
	precision := 0
	signVal := -1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetIntegerDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if false != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'\n", false, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'\n", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetIntegerDigits_04(t *testing.T) {

	nStr1 := "-0.4692"
	expected := "0"
	precision := 0
	signVal := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.GetIntegerDigits()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	if true != iAry2.IsZeroValue {
		t.Errorf("Error. Expected IsZeroValue= '%v'. Instead, got IsZeroValue='%v'", true, iAry2.IsZeroValue)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'", precision, iAry2.Precision)
	}

	if signVal != iAry2.SignVal {
		t.Errorf("Error. Expected SignVal= '%v'. Instead, got SignVal='%v'", signVal, iAry2.SignVal)
	}

}

func TestIntAry_GetNthRootOfThis_01(t *testing.T) {
	numStr1 := "125"
	nthRoot := uint(5)
	maxPrecision := uint(14)
	expected := "2.62652780440377"
	ia, _ := IntAry{}.NewNumStr(numStr1)
	iaResult, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia.GetNthRootOfThis(nthRoot, maxPrecision) - Error= %v", err)
	}

	if expected != iaResult.GetNumStr() {
		t.Errorf("Expected result= %v .  Instead iaResult.GetNumStr()= %v .", expected, iaResult.GetNumStr())
	}

}

func TestIntAry_GetNthRootOfThis_02(t *testing.T) {

	numStr1 := "5604423"
	nthRoot := uint(6)
	maxPrecision := uint(13)
	expected := "13.3276982415963"
	ia, _ := IntAry{}.NewNumStr(numStr1)
	iaResult, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from nRt.GetNthRootIntAry() - %v", err)
	}

	if expected != iaResult.GetNumStr() {
		t.Errorf("Expected result= %v .  Instead iaResult.GetNumStr()= %v .", expected, iaResult.GetNumStr())
	}

}

func TestIntAry_GetNthRootOfThis_03(t *testing.T) {

	numStr1 := "5604423.924"
	nthRoot := uint(6)
	maxPrecision := uint(13)
	expected := "13.3276986078187"
	ia, _ := IntAry{}.NewNumStr(numStr1)

	iaResult, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from iaResult.GetNthRootIntAry() - %v", err)
	}

	if expected != iaResult.GetNumStr() {
		t.Errorf("Expected result= %v .  Instead iaResult.GetNumStr()= %v .", expected, iaResult.GetNumStr())
	}

}

func TestIntAry_GetNthRootOfThis_04(t *testing.T) {

	numStr1 := "-27"
	nthRoot := uint(3)
	maxPrecision := uint(2)
	expected := "-3.00"
	ia, _ := IntAry{}.NewNumStr(numStr1)

	iaResult, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from iaResult.GetNthRootIntAry() - %v", err)
	}

	if expected != iaResult.GetNumStr() {
		t.Errorf("Expected result= %v .  Instead iaResult.GetNumStr()= %v .", expected, iaResult.GetNumStr())
	}

}

func TestIntAry_GetNthRootOfThis_05(t *testing.T) {
	ia := IntAry{}.New()
	numStr1 := "-27"
	nthRoot := uint(4)
	maxPrecision := uint(2)
	ia.SetIntAryWithNumStr(numStr1)
	_, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)

	if err == nil {
		t.Error("Expected Error from iaResult.GetNthRootIntAry() for negative number with even nthRoot. No Error triggered")
	}

}

func TestIntAry_GetNthRootOfThis_06(t *testing.T) {
	numStr1 := "-5604423.924"
	nthRoot := uint(5)
	maxPrecision := uint(13)
	expected := "-22.3720713464898"
	ia, _ := IntAry{}.NewNumStr(numStr1)

	iaResult, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from nRt.GetNthRootIntAry() - %v", err)
	}

	if expected != iaResult.GetNumStr() {
		t.Errorf("Expected result= %v .  Instead iaResult.GetNumStr()= %v .", expected, iaResult.GetNumStr())
	}

}

func TestIntAry_GetNthRootOfThis_07(t *testing.T) {

	numStr1 := "5604423.924"
	nthRoot := uint(0)
	maxPrecision := uint(1)
	expected := "1.0"
	ia, _ := IntAry{}.NewNumStr(numStr1)

	iaResult, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia.GetNthRootOfThis(nthRoot, maxPrecision) - %v", err)
	}

	if expected != iaResult.GetNumStr() {
		t.Errorf("Expected result= %v .  Instead iaResult.GetNumStr()= %v .", expected, iaResult.GetNumStr())
	}

}

func TestIntAry_GetNthRootOfThis_08(t *testing.T) {
	numStr1 := "27"
	ia, _ := IntAry{}.NewNumStr(numStr1)
	nthRoot := uint(1)
	maxPrecision := uint(2)
	_, err := ia.GetNthRootOfThis(nthRoot, maxPrecision)
	if err == nil {
		t.Error("Expected Error from ia.GetNthRootOfThis(nthRoot, maxPrecision) for nthRoot == 1. No Error triggered")
	}

}

func TestIntAry_GetSquareRootInt_01(t *testing.T) {

	numSr1 := "2686.5"
	maxPrecision := uint(30)
	expected := "51.831457629512986714934518985668"
	ai, _ := IntAry{}.NewNumStr(numSr1)

	aiResult, err := ai.GetSquareRootOfThis(maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ai.GetSquareRootOfThis(30) - Error: %v", err)
	}

	if expected != aiResult.GetNumStr() {
		t.Errorf("Expected result= %v .  Instead ai.GetNumStr()= %v .", expected, aiResult.GetNumStr())
	}

	if int(maxPrecision) != aiResult.Precision {
		t.Errorf("Expected precision= %v .  Instead precision= %v .", maxPrecision, aiResult.Precision)
	}

}

func TestIntAry_IncrementIntegerOne_01(t *testing.T) {
	expected := "100.123"
	nStr1 := "-100.123"
	cycles := 200
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	for i := 0; i < cycles; i++ {
		ia.IncrementIntegerOne()
	}

	ia.ConvertIntAryToNumStr()

	if expected != ia.GetNumStr() {
		t.Errorf("Error - Expected numStr= '%v'. Instead, numStr= '%v'", expected, ia.GetNumStr())
	}

}

func TestIntAry_IncrementIntegerOne_02(t *testing.T) {
	nStr1 := "-2000"
	expected := "2000"
	cycles := 4000
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	for i := 0; i < cycles; i++ {
		ia.IncrementIntegerOne()
	}

	ia.ConvertIntAryToNumStr()

	if expected != ia.GetNumStr() {
		t.Errorf("Error - Expected numStr= '%v'. Instead, numStr= '%v'", expected, ia.GetNumStr())
	}

}

func TestIntAry_IncrementIntegerOne_03(t *testing.T) {
	nStr1 := "-2000.123"
	expected := "2000.123"
	cycles := 4000
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	for i := 0; i < cycles; i++ {
		ia.IncrementIntegerOne()
	}

	ia.ConvertIntAryToNumStr()

	if expected != ia.GetNumStr() {
		t.Errorf("Error - Expected numStr= '%v'. Instead, numStr= '%v'", expected, ia.GetNumStr())
	}

}

func TestIntAry_IncrementIntegerOne_04(t *testing.T) {
	nStr1 := "0"
	expected := "40"
	cycles := 40
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	for i := 0; i < cycles; i++ {
		ia.IncrementIntegerOne()
	}

	ia.ConvertIntAryToNumStr()

	if expected != ia.GetNumStr() {
		t.Errorf("Error - Expected numStr= '%v'. Instead, numStr= '%v'", expected, ia.GetNumStr())
	}

}

func TestIntAry_MultiplyByTenToPower_01(t *testing.T) {

	nStr := "457.3"
	eNumStr := "45730"
	nRunes := []rune("45730")
	eIAry := []int{4, 5, 7, 3, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.MultiplyByTenToPower(2, false)
	ia.ConvertIntAryToNumStr()

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_MultiplyByTenToPower_02(t *testing.T) {

	nStr := "457.3"
	power := uint(2)
	eNumStr := "45730"
	nRunes := []rune("45730")
	eIAry := []int{4, 5, 7, 3, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.MultiplyByTenToPower(power, true)

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_MultiplyByTenToPower_03(t *testing.T) {

	nStr := "457.3"
	power := uint(10)
	eNumStr := "4573000000000"
	nRunes := []rune("4573000000000")
	eIAry := []int{4, 5, 7, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.MultiplyByTenToPower(power, true)

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_MultiplyByTenToPower_04(t *testing.T) {

	nStr := "457.3"
	power := uint(0)
	eNumStr := "457.3"
	nRunes := []rune("4573")
	eIAry := []int{4, 5, 7, 3}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 1
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.MultiplyByTenToPower(power, false)
	ia.ConvertIntAryToNumStr()

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_MultiplyByTenToPower_05(t *testing.T) {

	nStr := "-457.3"
	power := uint(1)
	eNumStr := "-4573"
	nRunes := []rune("4573")
	eIAry := []int{4, 5, 7, 3}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.MultiplyByTenToPower(power, false)
	ia.ConvertIntAryToNumStr()

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_MultiplyByTenToPower_06(t *testing.T) {

	nStr := "0"
	power := uint(2)
	eNumStr := "000"
	nRunes := []rune("000")
	eIAry := []int{0, 0, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.MultiplyByTenToPower(power, true)

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_MultiplyByTwoToPower_01(t *testing.T) {
	nStr1 := "23"
	expected := "12058624"
	power := uint(19)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.MultiplyByTwoToPower(power, true)

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= %v. Instead, ia.GetNumStr()= %v", expected, ia.GetNumStr())
	}

}

func TestIntAry_MultiplyByTwoToPower_02(t *testing.T) {
	nStr1 := "23"
	expected := "23"
	power := uint(0)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.MultiplyByTwoToPower(power, true)

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= %v. Instead, ia.GetNumStr()= %v", expected, ia.GetNumStr())
	}

}

func TestIntAry_MultiplyByTwoToPower_03(t *testing.T) {
	nStr1 := "23"
	expected := "46"
	power := uint(1)
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.MultiplyByTwoToPower(power, true)

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= %v. Instead, ia.GetNumStr()= %v", expected, ia.GetNumStr())
	}

}

func TestIntAry_NewNumStr_01(t *testing.T) {
	nStr1 := "579.123456000"
	ePrecision := 9
	eSignVal := 1

	ia, err := IntAry{}.NewNumStr(nStr1)

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewNumStr(nStr1). Error= %v", err)
	}

	if nStr1 != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", nStr1, ia.GetNumStr())
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewNumStr_02(t *testing.T) {
	nStr1 := "-579.123456000"
	ePrecision := 9
	eSignVal := -1

	ia, err := IntAry{}.NewNumStr(nStr1)

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewNumStr(nStr1). Error= %v", err)
	}

	if nStr1 != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", nStr1, ia.GetNumStr())
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewBigInt_01(t *testing.T) {
	num := big.NewInt(123456)
	precision := uint(3)
	ia, err := IntAry{}.NewBigInt(num, precision)
	eStr := "123.456"
	eSignVal := 1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewBigInt(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewBigInt_02(t *testing.T) {
	num := big.NewInt(-123456)
	precision := uint(3)
	ia, err := IntAry{}.NewBigInt(num, precision)
	eStr := "-123.456"
	eSignVal := -1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewBigInt(num, precision). num= %v  precision= %v Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewFloatBig_01(t *testing.T) {
	num := big.NewFloat(123.456)
	precision := uint(3)
	ia, err := IntAry{}.NewFloatBig(num)
	eStr := "123.456"
	eSignVal := 1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewFloatBig(num). num= %v Error= %v", num, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewFloatBig_02(t *testing.T) {
	num := big.NewFloat(-123.456)
	precision := uint(3)
	ia, err := IntAry{}.NewFloatBig(num)
	eStr := "-123.456"
	eSignVal := -1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewFloatBig(num). num= %v Error= %v", num, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewFloat64_01(t *testing.T) {
	num := float64(123.456111)
	precision := 3
	ia, err := IntAry{}.NewFloat64(num, precision)
	eStr := "123.456"
	eSignVal := 1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewFloat64(num, precision). num= %v precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}
}

func TestIntAry_NewFloat64_02(t *testing.T) {
	num := float64(-123.456111)
	precision := 3
	ia, err := IntAry{}.NewFloat64(num, precision)
	eStr := "-123.456"
	eSignVal := -1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewFloat64(num, precision). num= %v precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}
}

func TestIntAry_NewFloat32_01(t *testing.T) {
	num := float64(123.456111)
	precision := 3
	ia, err := IntAry{}.NewFloat64(num, precision)
	eStr := "123.456"
	eSignVal := 1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewFloat32(num, precision). num= %v precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}
}

func TestIntAry_NewFloat32_02(t *testing.T) {
	num := float64(-123.456111)
	precision := 3
	ia, err := IntAry{}.NewFloat64(num, precision)
	eStr := "-123.456"
	eSignVal := -1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewFloat32(num, precision). num= %v precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}
}

func TestIntAry_OptimizeIntArrayLen_01(t *testing.T) {
	nStr1 := "00579.123456000"
	expected := "579.123456000"
	ePrecision := 9
	eLen := 12

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.OptimizeIntArrayLen(false, true)

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.GetNumStr()= '%v'", expected, ia.GetNumStr())
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .   Instead, ia.IntAryLen= '%v' .", eLen, ia.IntAryLen)
	}

}

func TestIntAry_NewInt64_01(t *testing.T) {
	num := int64(123456)
	precision := uint(3)
	ia, err := IntAry{}.NewInt64(num, precision)
	eStr := "123.456"
	eSignVal := 1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewInt64(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewInt64_02(t *testing.T) {
	num := int64(-123456)
	precision := uint(3)
	ia, err := IntAry{}.NewInt64(num, precision)
	eStr := "-123.456"
	eSignVal := -1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewInt64(num, precision). num= %v  precision= %v Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewInt32_01(t *testing.T) {
	num := int32(123456)
	precision := uint(3)
	ia, err := IntAry{}.NewInt32(num, precision)
	eStr := "123.456"
	eSignVal := 1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewInt32(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewInt32_02(t *testing.T) {
	num := int32(-123456)
	precision := uint(3)
	ia, err := IntAry{}.NewInt32(num, precision)
	eStr := "-123.456"
	eSignVal := -1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewInt32(num, precision). num= %v  precision= %v Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewInt_01(t *testing.T) {
	num := int(123456)
	precision := uint(3)
	ia, err := IntAry{}.NewInt(num, precision)
	eStr := "123.456"
	eSignVal := 1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewInt(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_NewInt_02(t *testing.T) {
	num := int(-123456)
	precision := uint(3)
	ia, err := IntAry{}.NewInt(num, precision)
	eStr := "-123.456"
	eSignVal := -1

	if err != nil {
		t.Errorf("Error returned from IntAry{}.NewInt32(num, precision). num= %v  precision= %v Error= %v", num, precision, err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()== %v  .   Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if int(precision) != ia.Precision {
		t.Errorf("Expected ia.Precision == %v  .   Instead ia.Precision == %v", precision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v  .   Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_OptimizeIntArrayLen_02(t *testing.T) {
	nStr1 := "00579.123456000"
	expected := "579.123456"
	ePrecision := 6
	eLen := 9

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.OptimizeIntArrayLen(true, true)
	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.GetNumStr()= '%v'", expected, ia.GetNumStr())
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .   Instead, ia.IntAryLen= '%v' .", eLen, ia.IntAryLen)
	}

}

func TestIntAry_OptimizeIntArrayLen_03(t *testing.T) {
	nStr1 := "00579.000000000"
	expected := "579"
	ePrecision := 0
	eLen := 3

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.OptimizeIntArrayLen(true, true)
	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.GetNumStr()= '%v'", expected, ia.GetNumStr())
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .   Instead, ia.IntAryLen= '%v' .", eLen, ia.IntAryLen)
	}

}

func TestIntAry_OptimizeIntArrayLen_04(t *testing.T) {
	nStr1 := "00000.123450000"
	expected := "0.12345"
	ePrecision := 5
	eLen := 6

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.OptimizeIntArrayLen(true, true)
	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.GetNumStr()= '%v'", expected, ia.GetNumStr())
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .   Instead, ia.IntAryLen= '%v' .", eLen, ia.IntAryLen)
	}

}

func TestIntAry_OptimizeIntArrayLen_05(t *testing.T) {
	nStr1 := "00000.000123450000"
	expected := "0.00012345"
	ePrecision := 8
	eLen := 9

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.OptimizeIntArrayLen(true, true)
	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.GetNumStr()= '%v'", expected, ia.GetNumStr())
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v' .", ePrecision, ia.Precision)
	}

	if eLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .   Instead, ia.IntAryLen= '%v' .", eLen, ia.IntAryLen)
	}

}

func TestIntAry_RaiseToPower_01(t *testing.T) {

	nStr1 := "3"
	power := 3
	eStr := "27"
	eSignVal := 1
	ePrecision := 0

	ia := IntAry{}.New()

	ia.SetIntAryWithNumStr(nStr1)

	err := ia.RaiseThisToPower(power)

	if err != nil {
		t.Errorf("Error returned from ia.RaiseThisToPower(power). Error= %v", err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr() == %v . Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_RaiseToPower_02(t *testing.T) {

	nStr1 := "-3"
	power := 3
	eStr := "-27"
	eSignVal := -1
	ePrecision := 0

	ia := IntAry{}.New()

	ia.SetIntAryWithNumStr(nStr1)

	err := ia.RaiseThisToPower(power)

	if err != nil {
		t.Errorf("Error returned from ia.RaiseThisToPower(power). Error= %v", err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr() == %v . Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_RaiseToPower_03(t *testing.T) {

	nStr1 := "-3.97"
	power := 4
	eStr := "248.40596881"
	eSignVal := 1
	ePrecision := 8

	ia := IntAry{}.New()

	ia.SetIntAryWithNumStr(nStr1)

	err := ia.RaiseThisToPower(power)

	if err != nil {
		t.Errorf("Error returned from ia.RaiseThisToPower(power). Error= %v", err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr() == %v . Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_RaiseToPower_04(t *testing.T) {

	nStr1 := "-3.97"
	power := 3
	eStr := "-62.570773"
	eSignVal := -1
	ePrecision := 6

	ia := IntAry{}.New()

	ia.SetIntAryWithNumStr(nStr1)

	err := ia.RaiseThisToPower(power)

	if err != nil {
		t.Errorf("Error returned from ia.RaiseThisToPower(power). Error= %v", err)
	}

	if eStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr() == %v . Instead ia.GetNumStr() == %v", eStr, ia.GetNumStr())
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

	if ePrecision != ia.Precision {
		t.Errorf("Expected ia.SignVal == %v . Instead ia.SignVal == %v", eSignVal, ia.SignVal)
	}

}

func TestIntAry_ResetFromBackUp_01(t *testing.T) {
	nStr1 := "99.4564"
	nStr2 := "-5034.123"
	eSign := 1
	ePrecsion := 4
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.CopyToBackUp()
	ia.SetIntAryWithNumStr(nStr2)

	if nStr2 != ia.GetNumStr() {
		t.Errorf("Expected ia.NumStr2= '%v' .  Instead, ia.NumStr2= '%v' .", nStr2, ia.GetNumStr())
	}

	ia.ResetFromBackUp()

	if nStr1 != ia.GetNumStr() {
		t.Errorf("After Reset - Expected ia.NumStr1= '%v' .  Instead, ia.NumStr1= '%v' .", nStr1, ia.GetNumStr())
	}

	if ia.SignVal != eSign {
		t.Errorf("After Reset, Expected ia.SignVal= '%v'. Instead, ia.SignVal= '%v' ", eSign, ia.SignVal)
	}

	if ia.Precision != ePrecsion {
		t.Errorf("After Reset, Expected ia.Precision= '%v'. Instead, ia.Precision= '%v' .", ePrecsion, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_01(t *testing.T) {
	nStr1 := "999.9952"
	expected := "1000.00"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_02(t *testing.T) {
	nStr1 := "-999.9952"
	expected := "-1000.00"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_03(t *testing.T) {
	nStr1 := "352.954"
	expected := "352.95"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_04(t *testing.T) {
	nStr1 := "-352.954"
	expected := "-352.95"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_05(t *testing.T) {
	nStr1 := "-352.954"
	expected := "-352.95"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_06(t *testing.T) {
	nStr1 := "999.99"
	expected := "999.99"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_07(t *testing.T) {
	nStr1 := "999.99"
	expected := "999.99000"
	precision := 5
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_08(t *testing.T) {
	nStr1 := "0.000"
	expected := "0.00"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_09(t *testing.T) {
	nStr1 := "999.995"
	expected := "999.995"
	precision := 3
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_RoundToPrecision_10(t *testing.T) {
	nStr1 := "51.821810080312709972402243542716917"
	expected := "51.821810080312709972402243542717"
	precision := 30
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.RoundToPrecision(precision)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}
