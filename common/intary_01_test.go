package common

import (
"testing"
)

func TestIntAry_AddMultipleToThis_01(t *testing.T) {
	
	nStr1 := "457.3"
	nStr2 := "82.975"
	nStr3 := "94"
	nStr4 := "697.21589"
	nStr5 := "9648.37"

	expected := "10979.86089"

	ia0 := IntAry{}.New()
	ia0.SetIntAryToZero(0)
	
	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	
	ia3 := IntAry{}.New()
	ia3.SetIntAryWithNumStr(nStr3)
	
	ia4 := IntAry{}.New()
	ia4.SetIntAryWithNumStr(nStr4)
	
	ia5 := IntAry{}.New()
	ia5.SetIntAryWithNumStr(nStr5)
	
	ia0.AddMultipleToThis(true, &ia1, &ia2, &ia3, &ia4, &ia5)

	if expected != ia0.GetNumStr() {
		t.Errorf("Error: Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' . ", expected, ia0.GetNumStr())
	}

}

func TestIntAry_AddToThis_01(t *testing.T) {
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "457.3"
	nStr2 := "22.2"
	expected := "479.5"
	nRunes := []rune("4795")
	eIAry := []int{4, 7, 9, 5}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 1
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from mOps.AddN1N2(). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_02(t *testing.T) {
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "457.325"
	nStr2 := "-22.2"
	expected := "435.125"
	nRunes := []rune("435125")
	eIAry := []int{4, 3, 5, 1, 2, 5}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 3
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_03(t *testing.T) {
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-457.325"
	nStr2 := "22.2"
	expected := "-435.125"
	nRunes := []rune("435125")
	eIAry := []int{4, 3, 5, 1, 2, 5}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 3
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_04(t *testing.T) {
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-457.325"
	nStr2 := "-22.2"
	expected := "-479.525"
	nRunes := []rune("479525")
	eIAry := []int{4, 7, 9, 5, 2, 5}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 3
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_05(t *testing.T) {
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "0.000"
	nStr2 := "-22.2"
	expected := "-22.200"
	nRunes := []rune("22200")
	eIAry := []int{2, 2, 2, 0, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 3
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_06(t *testing.T) {
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "0.000"
	nStr2 := "0"
	expected := "0.000"
	nRunes := []rune("0000")
	eIAry := []int{0, 0, 0, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 3
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_07(t *testing.T) {
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "99.225"
	nStr2 := "-99.1"
	expected := "0.125"
	nRunes := []rune("0125")
	eIAry := []int{0, 1, 2, 5}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 3
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_08(t *testing.T) {
	// N1 > N2 + and +
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "350"
	nStr2 := "122"
	expected := "472"
	nRunes := []rune("472")
	eIAry := []int{4, 7, 2}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_09(t *testing.T) {
	// N1 > N2 - and +
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-350"
	nStr2 := "122"
	expected := "-228"
	nRunes := []rune("228")
	eIAry := []int{2, 2, 8}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_10(t *testing.T) {
	// N1 > N2 - and -
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-350"
	nStr2 := "-122"
	expected := "-472"
	nRunes := []rune("472")
	eIAry := []int{4, 7, 2}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_11(t *testing.T) {
	// N1 > N2 + and -
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "350"
	nStr2 := "-122"
	expected := "228"
	nRunes := []rune("228")
	eIAry := []int{2, 2, 8}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_12(t *testing.T) {
	// N1 > N2  350 + 0
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "350"
	nStr2 := "0"
	expected := "350"
	nRunes := []rune("350")
	eIAry := []int{3, 5, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_13(t *testing.T) {
	// N1 > N2  -350 + 0
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-350"
	nStr2 := "0"
	expected := "-350"
	nRunes := []rune("350")
	eIAry := []int{3, 5, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_14(t *testing.T) {
	// N2 > N1  + and +
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "122"
	nStr2 := "350"
	expected := "472"
	nRunes := []rune("472")
	eIAry := []int{4, 7, 2}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_15(t *testing.T) {
	// N2 > N1  - and +
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-122"
	nStr2 := "350"
	expected := "228"
	nRunes := []rune("228")
	eIAry := []int{2, 2, 8}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_16(t *testing.T) {
	// N2 > N1  - and -
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-122"
	nStr2 := "-350"
	expected := "-472"
	nRunes := []rune("472")
	eIAry := []int{4, 7, 2}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_17(t *testing.T) {
	// N2 > N1  + and -
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "122"
	nStr2 := "-350"
	expected := "-228"
	nRunes := []rune("228")
	eIAry := []int{2, 2, 8}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_18(t *testing.T) {
	// N2 > N1  0 and +350
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "0"
	nStr2 := "350"
	expected := "350"
	nRunes := []rune("350")
	eIAry := []int{3, 5, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_19(t *testing.T) {
	// N2 > N1  0 and -350
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "0"
	nStr2 := "-350"
	expected := "-350"
	nRunes := []rune("350")
	eIAry := []int{3, 5, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_20(t *testing.T) {
	// N1 == N2  +122 and +122
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "122"
	nStr2 := "122"
	expected := "244"
	nRunes := []rune("244")
	eIAry := []int{2, 4, 4}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_21(t *testing.T) {
	// N1 == N2  -122 and +122
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-122"
	nStr2 := "122"
	expected := "0"
	nRunes := []rune("0")
	eIAry := []int{0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_22(t *testing.T) {
	// N1 == N2  -122 and -122
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "-122"
	nStr2 := "-122"
	expected := "-244"
	nRunes := []rune("244")
	eIAry := []int{2, 4, 4}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := -1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_23(t *testing.T) {
	// N1 == N2  +122 and -122
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "122"
	nStr2 := "-122"
	expected := "0"
	nRunes := []rune("0")
	eIAry := []int{0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_AddToThis_24(t *testing.T) {
	// N1 == N2  0 and 0
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	nStr1 := "0"
	nStr2 := "0"
	expected := "0"
	nRunes := []rune("0")
	eIAry := []int{0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 0
	eSignVal := 1

	ia1.SetIntAryWithNumStr(nStr1)

	ia2.SetIntAryWithNumStr(nStr2)

	err := ia1.AddToThis(&ia2, true)

	if err != nil {
		t.Errorf("Received Error from ia1.AddToThis(&ia2, true). nStr1= '%v' nStr2= '%v' Error= %v", nStr1, nStr2, err)
	}

	s := ia1.GetNumStr()

	if s != expected {
		t.Errorf("Expected IFinal.GetNumStr()= '%v'. Instead got IFinal.Numstr= '%v' ", expected, s)
	}

	if ia1.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia1.SignVal)
	}

	if lNRunes != ia1.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia1.NumRunesLen)
	}

	if lEArray != ia1.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia1.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia1.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia1.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_Ceiling_01(t *testing.T) {
	nStr1 := "0.925"
	expected := "1.000"
	precision := 3

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Ceiling()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Ceiling_02(t *testing.T) {
	nStr1 := "-2.7"
	expected := "-2.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Ceiling()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Ceiling_03(t *testing.T) {
	nStr1 := "2.9"
	expected := "3.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Ceiling()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}
func TestIntAry_Ceiling_04(t *testing.T) {
	nStr1 := "2.0"
	expected := "2.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Ceiling()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Ceiling_05(t *testing.T) {
	nStr1 := "2.4"
	expected := "3.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Ceiling()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Ceiling_06(t *testing.T) {
	nStr1 := "2.9"
	expected := "3.0"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Ceiling()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_Ceiling_07(t *testing.T) {
	nStr1 := "-2"
	expected := "-2"
	precision := 0

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	iAry2, err := ia.Ceiling()

	if err != nil {
		t.Errorf("Received Error from ia.Ceiling(). Error:= %v", err)
	}

	s := iAry2.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if iAry2.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, iAry2.Precision)
	}

}

func TestIntAry_CompareSignedValues_01(t *testing.T) {

	nStr1 := "451.3"
	nStr2 := "451.2"
	expectedCompare := 1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_02(t *testing.T) {

	nStr1 := "45.13"
	nStr2 := "451.2"
	expectedCompare := -1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_03(t *testing.T) {

	nStr1 := "45.13975"
	nStr2 := "-451.21"
	expectedCompare := 1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_04(t *testing.T) {

	nStr1 := "0"
	nStr2 := "0.00"
	expectedCompare := 0

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_05(t *testing.T) {

	nStr1 := "-625.414"
	nStr2 := "-625.413"
	expectedCompare := -1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_06(t *testing.T) {

	nStr1 := "625.414"
	nStr2 := "625.413000"
	expectedCompare := 1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_07(t *testing.T) {

	nStr1 := "625.413"
	nStr2 := "625.413001"
	expectedCompare := -1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_08(t *testing.T) {

	nStr1 := "625.413"
	nStr2 := "625.413000"
	expectedCompare := 0

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_09(t *testing.T) {

	nStr1 := "625.413"
	nStr2 := "00625.413000"
	expectedCompare := 0

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_10(t *testing.T) {

	nStr1 := "625.413"
	nStr2 := "-00625.413000"
	expectedCompare := 1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_11(t *testing.T) {

	nStr1 := "-625.413"
	nStr2 := "-00625.413000"
	expectedCompare := 0

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareSignedValues_12(t *testing.T) {

	nStr1 := "-625.413"
	nStr2 := "00625.413000"
	expectedCompare := -1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareSignedValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareAbsoluteValues_01(t *testing.T) {

	nStr1 := "45.13"
	nStr2 := "451.2"
	expectedCompare := -1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareAbsoluteValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareAbsoluteValues_02(t *testing.T) {

	nStr1 := "45.13"
	nStr2 := "-451.2"
	expectedCompare := -1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareAbsoluteValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareAbsoluteValues_03(t *testing.T) {

	nStr1 := "-45.13"
	nStr2 := "45.13"
	expectedCompare := 0

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareAbsoluteValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareAbsoluteValues_04(t *testing.T) {

	nStr1 := "-45.13000"
	nStr2 := "45.13"
	expectedCompare := 0

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareAbsoluteValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareAbsoluteValues_05(t *testing.T) {

	nStr1 := "-45.13001"
	nStr2 := "45.13"
	expectedCompare := 1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareAbsoluteValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CompareAbsoluteValues_06(t *testing.T) {

	nStr1 := "-45.1300"
	nStr2 := "452"
	expectedCompare := -1

	ia1 := IntAry{}.New()

	ia1.SetIntAryWithNumStr(nStr1)

	ia2 := IntAry{}.New()

	ia2.SetIntAryWithNumStr(nStr2)

	actualCompare := ia1.CompareAbsoluteValues(&ia2)

	if actualCompare != expectedCompare {
		t.Errorf("Error. Expected Comparison Result= '%v'. Instead, Comparison Result= '%v'", expectedCompare, actualCompare)
	}

}

func TestIntAry_CopyIn_01(t *testing.T) {

	nStr1 := "00100.1230"
	nStr2 := "-0000052.795813000"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	if nStr1 != ia.GetNumStr() {
		t.Errorf("Error ia.SetIntAryWithNumStr(nStr1). Expected ia.GetNumStr()= '%v'  .   Instead, ia.GetNumStr()= '%v' .",nStr1, ia.GetNumStr())
	}

	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)

	if nStr2 != ia2.GetNumStr() {
		t.Errorf("Error ia2.SetIntAryWithNumStr(nStr2). Expected ia.GetNumStr()= '%v'  .   Instead, ia.GetNumStr()= '%v' .",nStr2, ia2.GetNumStr())
	}

	ia.CopyIn(&ia2, false)

	if ia2.GetNumStr() != ia.GetNumStr() {
		t.Errorf("Error Expected ia.GetNumStr()= '%v'  .   Instead, ia.GetNumStr()= '%v' .",ia2.GetNumStr(), ia.GetNumStr())
	}

	if ia2.SignVal != ia.SignVal {
		t.Errorf("Error Expcted ia.SignVal= '%v'  .   Instead, ia.SignVal= '%v' .",ia2.SignVal, ia.SignVal)
	}

	if ia2.Precision != ia.Precision {
		t.Errorf("Error Expcted ia.Precision= '%v'  .   Instead, ia.Precision= '%v' .",ia2.Precision, ia.Precision)
	}

	if ia2.IntAryLen != ia.IntAryLen {
		t.Errorf("Error Expcted ia.IntAryLen= '%v'  .   Instead, ia.IntAryLen= '%v' .",ia2.IntAryLen, ia.IntAryLen)
	}

	if ia2.IntegerLen != ia.IntegerLen {
		t.Errorf("Error Expcted ia.IntegerLen= '%v'  .   Instead, ia.IntegerLen= '%v' .",ia2.IntegerLen, ia.IntegerLen)
	}

	if ia2.SignificantIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Error Expcted ia.SignificantIntegerLen= '%v'  .   Instead, ia.SignificantIntegerLen= '%v' .",ia2.SignificantIntegerLen, ia.SignificantIntegerLen)
	}

	if ia2.SignificantFractionLen != ia.SignificantFractionLen {
		t.Errorf("Error Expcted ia.SignificantFractionLen= '%v'  .   Instead, ia.SignificantFractionLen= '%v' .",ia2.SignificantFractionLen, ia.SignificantFractionLen)
	}

	if ia2.FirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Error Expcted ia.FirstDigitIdx= '%v'  .   Instead, ia.FirstDigitIdx= '%v' .",ia2.FirstDigitIdx, ia.FirstDigitIdx)
	}

	if ia2.LastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Error Expcted ia.LastDigitIdx= '%v'  .   Instead, ia.LastDigitIdx= '%v' .",ia2.LastDigitIdx, ia.LastDigitIdx)
	}

	if ia2.IsZeroValue != ia.IsZeroValue {
		t.Errorf("Error Expcted ia.IsZeroValue= '%v'  .   Instead, ia.IsZeroValue= '%v' .",ia2.IsZeroValue, ia.IsZeroValue)
	}

	if ia2.IsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Error Expcted ia.IsIntegerZeroValue= '%v'  .   Instead, ia.IsIntegerZeroValue= '%v' .",ia2.IsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if ia2.DecimalSeparator != '.' {
		t.Errorf("Error Expcted ia.DecimalSeparator= '%v'  .   Instead, ia.DecimalSeparator= '%v' .",'.', ia.DecimalSeparator)
	}

	for i:=0; i < ia.IntAryLen; i++ {

		if ia2.IntAry[i] != ia.IntAry[i] {
			t.Errorf("Error Expcted ia.IntAry[i]= '%v'  .   Instead, ia.IntAry[i]= '%v' .",ia2.IntAry[i], ia.IntAry[i])
		}
	}

	if ia2.NumRunesLen != ia.NumRunesLen {
		t.Errorf("Error Expcted ia.NumRunesLen= '%v'  .   Instead, ia.NumRunesLen= '%v' .",ia2.NumRunesLen, ia.NumRunesLen)
	}

	for j:=0; j < ia.NumRunesLen; j++ {
		if ia.NumRunes[j] != ia2.NumRunes[j] {
			t.Errorf("Error Expcted ia.NumRunes[j]= '%v'  .   Instead, ia.NumRunes[j]= '%v' .",ia2.NumRunes[j], ia.NumRunes[j])
		}
	}

}

func TestIntAry_CopyIn_02(t *testing.T) {

	nStr1 := "00100.1230"
	nStr2 := "3594176.123459"


	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.CopyToBackUp()

	if nStr1 != ia.GetNumStr() {
		t.Errorf("Error ia.SetIntAryWithNumStr(nStr1). Expected ia.GetNumStr()= '%v'  .   Instead, ia.GetNumStr()= '%v' .",nStr1, ia.GetNumStr())
	}

	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia2.CopyToBackUp()


	if nStr2 != ia2.GetNumStr() {
		t.Errorf("Error ia2.SetIntAryWithNumStr(nStr2). Expected ia.GetNumStr()= '%v'  .   Instead, ia.GetNumStr()= '%v' .",nStr2, ia2.GetNumStr())
	}

	ia.CopyIn(&ia2, true)

	if ia2.GetNumStr() != ia.GetNumStr() {
		t.Errorf("Error Expected ia.GetNumStr()= '%v'  .   Instead, ia.GetNumStr()= '%v' .",ia2.GetNumStr(), ia.GetNumStr())
	}

	if ia2.SignVal != ia.SignVal {
		t.Errorf("Error Expcted ia.SignVal= '%v'  .   Instead, ia.SignVal= '%v' .",ia2.SignVal, ia.SignVal)
	}

	if ia2.Precision != ia.Precision {
		t.Errorf("Error Expcted ia.Precision= '%v'  .   Instead, ia.Precision= '%v' .",ia2.Precision, ia.Precision)
	}

	if ia2.IntAryLen != ia.IntAryLen {
		t.Errorf("Error Expcted ia.IntAryLen= '%v'  .   Instead, ia.IntAryLen= '%v' .",ia2.IntAryLen, ia.IntAryLen)
	}

	if ia2.IntegerLen != ia.IntegerLen {
		t.Errorf("Error Expcted ia.IntegerLen= '%v'  .   Instead, ia.IntegerLen= '%v' .",ia2.IntegerLen, ia.IntegerLen)
	}

	if ia2.SignificantIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Error Expcted ia.SignificantIntegerLen= '%v'  .   Instead, ia.SignificantIntegerLen= '%v' .",ia2.SignificantIntegerLen, ia.SignificantIntegerLen)
	}

	if ia2.SignificantFractionLen != ia.SignificantFractionLen {
		t.Errorf("Error Expcted ia.SignificantFractionLen= '%v'  .   Instead, ia.SignificantFractionLen= '%v' .",ia2.SignificantFractionLen, ia.SignificantFractionLen)
	}

	if ia2.FirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Error Expcted ia.FirstDigitIdx= '%v'  .   Instead, ia.FirstDigitIdx= '%v' .",ia2.FirstDigitIdx, ia.FirstDigitIdx)
	}

	if ia2.LastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Error Expcted ia.LastDigitIdx= '%v'  .   Instead, ia.LastDigitIdx= '%v' .",ia2.LastDigitIdx, ia.LastDigitIdx)
	}

	if ia2.IsZeroValue != ia.IsZeroValue {
		t.Errorf("Error Expcted ia.IsZeroValue= '%v'  .   Instead, ia.IsZeroValue= '%v' .",ia2.IsZeroValue, ia.IsZeroValue)
	}

	if ia2.IsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Error Expcted ia.IsIntegerZeroValue= '%v'  .   Instead, ia.IsIntegerZeroValue= '%v' .",ia2.IsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if ia2.DecimalSeparator != '.' {
		t.Errorf("Error Expcted ia.DecimalSeparator= '%v'  .   Instead, ia.DecimalSeparator= '%v' .",'.', ia.DecimalSeparator)
	}

	for i:=0; i < ia.IntAryLen; i++ {

		if ia2.IntAry[i] != ia.IntAry[i] {
			t.Errorf("Error Expcted ia.IntAry[i]= '%v'  .   Instead, ia.IntAry[i]= '%v' .",ia2.IntAry[i], ia.IntAry[i])
		}
	}

	if ia2.NumRunesLen != ia.NumRunesLen {
		t.Errorf("Error Expcted ia.NumRunesLen= '%v'  .   Instead, ia.NumRunesLen= '%v' .",ia2.NumRunesLen, ia.NumRunesLen)
	}

	for j:=0; j < ia.NumRunesLen; j++ {
		if ia.NumRunes[j] != ia2.NumRunes[j] {
			t.Errorf("Error Expcted ia.NumRunes[j]= '%v'  .   Instead, ia.NumRunes[j]= '%v' .",ia2.NumRunes[j], ia.NumRunes[j])
		}
	}

	if !ia.BackUp.Equals(&ia2.BackUp){
		t.Error("Error Expcted ia.Backup to Equal ia2.Backup.   ")
	}

}

func TestIntAry_CopyToBackUp_01(t *testing.T) {
	nStr1 := "100.123"
	nStr2 := "-52.795813"
	expected := "100.123"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.CopyToBackUp()
	ia.SetIntAryWithNumStr(nStr2)

	if nStr2 != ia.GetNumStr() {
		t.Errorf("Error - Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v' .", nStr2, ia.GetNumStr())
	}

	str := ia.BackUp.GetNumStr()

	if expected != str {
		t.Errorf("Error - Expected ia.BackUp.GetNumStr()= '%v' .  Instead, ia.BackUp.GetNumStr()= '%v' .", expected, str)
	}

}

func TestIntAry_DecrementIntegerOne_01(t *testing.T) {
	nStr1 := "100.123"
	expected := "-100.123"
	cycles := 200
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	for i := 0; i < cycles; i++ {
		ia.DecrementIntegerOne()
	}

	ia.ConvertIntAryToNumStr()

	if expected != ia.GetNumStr() {
		t.Errorf("Error - Expected numStr= '%v'. Instead, numStr= '%v'", expected, ia.GetNumStr())
	}

}

func TestIntAry_DecrementIntegerOne_02(t *testing.T) {
	nStr1 := "2000"
	expected := "-2000"
	cycles := 4000
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	for i := 0; i < cycles; i++ {
		ia.DecrementIntegerOne()
	}

	ia.ConvertIntAryToNumStr()

	if expected != ia.GetNumStr() {
		t.Errorf("Error - Expected numStr= '%v'. Instead, numStr= '%v'", expected, ia.GetNumStr())
	}

}

func TestIntAry_DecrementIntegerOne_03(t *testing.T) {
	nStr1 := "2000.123"
	expected := "-2000.123"
	cycles := 4000
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)

	for i := 0; i < cycles; i++ {
		ia.DecrementIntegerOne()
	}

	ia.ConvertIntAryToNumStr()

	if expected != ia.GetNumStr() {
		t.Errorf("Error - Expected numStr= '%v'. Instead, numStr= '%v'", expected, ia.GetNumStr())
	}

}

func TestIntAry_DivideByInt64_01(t *testing.T) {
	nStr1 := "579"
	expected := "17.545454545454545454545454545455"
	maxPrecision := uint64(30)
	divisor := int64(33)

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.DivideByInt64(divisor, maxPrecision, true )

	if err != nil {
		t.Errorf("Received Error from ia.DivideByInt64(divisor, maxPrecision, true ). Error= %v", err)
	}

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.Numstr= '%v'", expected, ia.GetNumStr())
	}

	if ia.Precision != int(maxPrecision) {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'.",maxPrecision, ia.Precision)
	}

}

func TestIntAry_DivideByInt64_02(t *testing.T) {
	nStr1 := "579"
	maxPrecision := uint64(0)
	divisor := int64(0)

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err:=ia.DivideByInt64(divisor, maxPrecision, true )

	if err == nil {
		t.Errorf("Expected Divide By Zero Error. No Error Presented. Error = %v", err)
	}

}

func TestIntAry_DivideByInt64_03(t *testing.T) {
	nStr1 := "4"
	expected := "2"
	maxPrecision := uint64(0)
	divisor := int64(2)

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.DivideByInt64(divisor, maxPrecision, true )

	if err != nil {
		t.Errorf("Received Error from ia.DivideByInt64(divisor, maxPrecision, true ). Error= %v", err)
	}

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.Numstr= '%v'", expected, ia.GetNumStr())
	}

	if ia.Precision != int(maxPrecision) {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'.",maxPrecision, ia.Precision)
	}

}

func TestIntAry_DivideByInt64_04(t *testing.T) {
	nStr1 := "476"
	expected := "-14"
	maxPrecision := uint64(10)
	ePrecision := 0
	eSignVal := -1
	divisor := int64(-34)

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.DivideByInt64(divisor, maxPrecision, true )

	if err != nil {
		t.Errorf("Received Error from ia.DivideByInt64(divisor, maxPrecision, true ). Error= %v", err)
	}

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.Numstr= '%v'", expected, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'.",ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v'. Instead, ia.Precision= '%v'.",eSignVal, ia.SignVal)
	}

}

func TestIntAry_DivideByInt64_05(t *testing.T) {
	nStr1 := "-476"
	expected := "-14"
	maxPrecision := uint64(10)
	ePrecision := 0
	eSignVal := -1
	divisor := int64(34)

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.DivideByInt64(divisor, maxPrecision, true )

	if err != nil {
		t.Errorf("Received Error from ia.DivideByInt64(divisor, maxPrecision, true ). Error= %v", err)
	}

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.Numstr= '%v'", expected, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'.",ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v'. Instead, ia.Precision= '%v'.",eSignVal, ia.SignVal)
	}

}

func TestIntAry_DivideByInt64_06(t *testing.T) {
	nStr1 := "-476"
	expected := "14"
	maxPrecision := uint64(10)
	ePrecision := 0
	eSignVal := 1
	divisor := int64(-34)

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.DivideByInt64(divisor, maxPrecision, true )

	if err != nil {
		t.Errorf("Received Error from ia.DivideByInt64(divisor, maxPrecision, true ). Error= %v", err)
	}

	if expected != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v'. Instead, ia.Numstr= '%v'", expected, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Expected ia.Precision= '%v'. Instead, ia.Precision= '%v'.",ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v'. Instead, ia.Precision= '%v'.",eSignVal, ia.SignVal)
	}

}

func TestIntAry_DivideByTwo_01(t *testing.T) {

	nStr1 := "1959"
	expected := "979.5"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.DivideByTwo(true)

	if expected != ia.GetNumStr() {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", expected, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", precision, ia.Precision)
	}

}

func TestIntAry_DivideByTwo_02(t *testing.T) {

	nStr1 := "1"
	expected := "0.5"
	precision := 1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.DivideByTwo(true)

	if expected != ia.GetNumStr() {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", expected, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", precision, ia.Precision)
	}

}

func TestIntAry_DivideByTwo_03(t *testing.T) {

	nStr1 := "0"
	expected := "0"
	precision := 0

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.DivideByTwo(true)

	if expected != ia.GetNumStr() {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", expected, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", precision, ia.Precision)
	}

}

func TestIntAry_DivideByTwo_04(t *testing.T) {

	nStr1 := "-2959"
	expected := "-1479.5"
	precision := 1
	signVal := -1

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	ia.DivideByTwo(true)

	if expected != ia.GetNumStr() {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", expected, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Error: Expected Sign Value= '%v'. Instead, Sign Value= '%v'. ", signVal, ia.SignVal)
	}

}

func TestIntAry_DivideByTenToPower_01(t *testing.T) {

	nStr := "457.3"
	power := uint(1)
	eNumStr := "45.73"
	nRunes := []rune("4573")
	eIAry := []int{4, 5, 7, 3}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 2
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.DivideByTenToPower(power, false)
	ia.ConvertIntAryToNumStr()

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}
}

func TestIntAry_DivideByTenToPower_02(t *testing.T) {

	nStr := "457.3"
	power := uint(3)
	eNumStr := "0.4573"
	nRunes := []rune("04573")
	eIAry := []int{0, 4, 5, 7, 3}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 4
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.DivideByTenToPower(power, false)
	ia.ConvertIntAryToNumStr()

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}
}

func TestIntAry_DivideByTenToPower_03(t *testing.T) {

	nStr := "457.3"
	power := uint(7)
	eNumStr := "0.00004573"
	nRunes := []rune("000004573")
	eIAry := []int{0, 0, 0, 0, 0, 4, 5, 7, 3}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 8
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.DivideByTenToPower(power, false)
	ia.ConvertIntAryToNumStr()

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}
}

func TestIntAry_DivideByTenToPower_04(t *testing.T) {

	nStr := "-457.3"
	power := uint(7)
	eNumStr := "-0.00004573"
	nRunes := []rune("000004573")
	eIAry := []int{0, 0, 0, 0, 0, 4, 5, 7, 3}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 8
	eSignVal := -1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.DivideByTenToPower(power, false)
	ia.ConvertIntAryToNumStr()

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}
}

func TestIntAry_DivideByTenToPower_05(t *testing.T) {

	nStr := "0"
	power := uint(2)
	eNumStr := "0.00"
	nRunes := []rune("000")
	eIAry := []int{0, 0, 0}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 2
	eSignVal := 1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.DivideByTenToPower(power, true)

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}
}

func TestIntAry_DivideByTenToPower_06(t *testing.T) {

	nStr := "-4573"
	power := uint(1)
	eNumStr := "-457.3"
	nRunes := []rune("4573")
	eIAry := []int{4, 5, 7, 3}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 1
	eSignVal := -1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.DivideByTenToPower(power, true)

	if ia.GetNumStr() != eNumStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", eNumStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}
}

func TestIntAry_DivideThisBy_01(t *testing.T) {
	dividend := "56234369384300"
	divisor :=  "24"
	eQuotient := "2343098724345.833333333333333333333"
	eSignVal := 1
	maxPrecision := 21
	ePrecision := 21

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}

}

func TestIntAry_DivideThisBy_02(t *testing.T) {
	dividend := "48"
	divisor :=  "24"
	eSignVal := 1
	eQuotient := "2"
	maxPrecision := 21
	ePrecision := 0

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}

}

func TestIntAry_DivideThisBy_03(t *testing.T) {
	dividend := "24"
	divisor :=  "24"
	eQuotient := "1"
	eSignVal := 1
	maxPrecision := 21
	ePrecision := 0

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}

}

func TestIntAry_DivideThisBy_04(t *testing.T) {
	dividend := "0.05"
	divisor :=  "24"
	eQuotient := "0.00208333333333333333333333333333"
	eSignVal := 1
	maxPrecision := 32
	ePrecision := 32

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}

}

func TestIntAry_DivideThisBy_05(t *testing.T) {
	dividend := "0"
	divisor :=  "24"
	eQuotient := "0"
	eSignVal := 1
	maxPrecision := 7
	ePrecision := 0

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}

}

func TestIntAry_DivideThisBy_06(t *testing.T) {
	dividend := "48"
	divisor :=  "0"

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	_, err := ia1.DivideThisBy(&ia2, 15)

	if err == nil {
		t.Error("Expected an error from Divideby Zero. No Error Received!")
	}

}

func TestIntAry_DivideThisBy_07(t *testing.T) {
	dividend := "-9360"
	divisor :=  "24.48"
	eQuotient := "-382.35294117647058823529411764706"
	eSignVal := -1
	maxPrecision := 29
	ePrecision := 29

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}
}

func TestIntAry_DivideThisBy_08(t *testing.T) {
	dividend := "-9360"
	divisor :=  "-24.48"
	eQuotient := "382.35294117647058823529411764706"
	eSignVal := 1
	maxPrecision := 29
	ePrecision := 29

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}
}

func TestIntAry_DivideThisBy_09(t *testing.T) {
	dividend := "9360"
	divisor :=  "-24.48"
	eQuotient := "-382.35294117647058823529411764706"
	eSignVal := -1
	maxPrecision := 29
	ePrecision := 29

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}
}

func TestIntAry_DivideThisBy_10(t *testing.T) {
	dividend := "-19260.549"
	divisor :=  "-246.483"
	eQuotient := "78.141490488187826340964691276883"
	eSignVal := 1
	maxPrecision := 30
	ePrecision := 30

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(dividend)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(divisor)

	quotient, err := ia1.DivideThisBy(&ia2, maxPrecision)

	if err != nil {
		t.Errorf("Error returned from ia1.DivideThisBy(&ia2, maxPrecision). Error= %v", err)
	}

	if eQuotient != quotient.GetNumStr() {
		t.Errorf("Expected quotient.GetNumStr()= '%v' .  Instead, quotient.GetNumStr()= '%v'  .", eQuotient, quotient.GetNumStr())
	}

	if ePrecision != quotient.Precision {
		t.Errorf("Expected quotient.Precision= '%v' .  Instead, quotient.Precision= '%v'  .", ePrecision, quotient.Precision)
	}

	if eSignVal != quotient.SignVal {
		t.Errorf("Error - Expected smop.Quotient.SignVal= '%v'. Instead, smop.Quotient.SignVal= '%v' .", eSignVal, quotient.SignVal)
	}
}

