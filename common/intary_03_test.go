package common

import (
	"math/big"
	"testing"
)


func TestIntAry_SetEqualArrayLengths_01(t *testing.T) {
	nStr1 := "3536.123456"
	eNStr1 := "3536.123456"
	ePrecision1 := 6
	eSignVal1 := 1

	nStr2 := "12.14"
	eNStr2 := "0012.140000"
	ePrecision2 := 6
	eSignVal2 := 1

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia1.SetEqualArrayLengths(&ia2)

	if ia1.GetNumStr() != eNStr1 {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' . Instead, ia1.GetNumStr()= '%v' .", eNStr1, ia1.GetNumStr())
	}

	if ia1.SignVal != eSignVal1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", eSignVal1, ia1.SignVal)
	}

	if ia1.Precision != ePrecision1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", ePrecision1, ia1.Precision)
	}

	if ia2.GetNumStr() != eNStr2 {
		t.Errorf("Error - Expected ia2.GetNumStr()= '%v' . Instead, ia2.GetNumStr()= '%v' .", eNStr2, ia2.GetNumStr())
	}

	if ia2.SignVal != eSignVal2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", eSignVal2, ia2.SignVal)
	}

	if ia2.Precision != ePrecision2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", ePrecision2, ia2.Precision)
	}

}

func TestIntAry_SetEqualArrayLengths_02(t *testing.T) {
	nStr1 := "12.14"
	eNStr1 := "0012.140000"
	ePrecision1 := 6
	eSignVal1 := 1

	nStr2 := "3536.123456"
	eNStr2 := "3536.123456"
	ePrecision2 := 6
	eSignVal2 := 1

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia1.SetEqualArrayLengths(&ia2)

	if ia1.GetNumStr() != eNStr1 {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' . Instead, ia1.GetNumStr()= '%v' .", eNStr1, ia1.GetNumStr())
	}

	if ia1.SignVal != eSignVal1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", eSignVal1, ia1.SignVal)
	}

	if ia1.Precision != ePrecision1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", ePrecision1, ia1.Precision)
	}

	if ia2.GetNumStr() != eNStr2 {
		t.Errorf("Error - Expected ia2.GetNumStr()= '%v' . Instead, ia2.GetNumStr()= '%v' .", eNStr2, ia2.GetNumStr())
	}

	if ia2.SignVal != eSignVal2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", eSignVal2, ia2.SignVal)
	}

	if ia2.Precision != ePrecision2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", ePrecision2, ia2.Precision)
	}

}

func TestIntAry_SetEqualArrayLengths_03(t *testing.T) {
	nStr1 := "3536.123456"
	eNStr1 := "3536.123456"
	ePrecision1 := 6
	eSignVal1 := 1

	nStr2 := "-12.14"
	eNStr2 := "-0012.140000"
	ePrecision2 := 6
	eSignVal2 := -1

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia1.SetEqualArrayLengths(&ia2)

	if ia1.GetNumStr() != eNStr1 {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' . Instead, ia1.GetNumStr()= '%v' .", eNStr1, ia1.GetNumStr())
	}

	if ia1.SignVal != eSignVal1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", eSignVal1, ia1.SignVal)
	}

	if ia1.Precision != ePrecision1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", ePrecision1, ia1.Precision)
	}

	if ia2.GetNumStr() != eNStr2 {
		t.Errorf("Error - Expected ia2.GetNumStr()= '%v' . Instead, ia2.GetNumStr()= '%v' .", eNStr2, ia2.GetNumStr())
	}

	if ia2.SignVal != eSignVal2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", eSignVal2, ia2.SignVal)
	}

	if ia2.Precision != ePrecision2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", ePrecision2, ia2.Precision)
	}

}

func TestIntAry_SetEqualArrayLengths_04(t *testing.T) {
	nStr1 := "-12.14"
	eNStr1 := "-0012.140000"
	ePrecision1 := 6
	eSignVal1 := -1

	nStr2 := "3536.123456"
	eNStr2 := "3536.123456"
	ePrecision2 := 6
	eSignVal2 := 1

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia1.SetEqualArrayLengths(&ia2)

	if ia1.GetNumStr() != eNStr1 {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' . Instead, ia1.GetNumStr()= '%v' .", eNStr1, ia1.GetNumStr())
	}

	if ia1.SignVal != eSignVal1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", eSignVal1, ia1.SignVal)
	}

	if ia1.Precision != ePrecision1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", ePrecision1, ia1.Precision)
	}

	if ia2.GetNumStr() != eNStr2 {
		t.Errorf("Error - Expected ia2.GetNumStr()= '%v' . Instead, ia2.GetNumStr()= '%v' .", eNStr2, ia2.GetNumStr())
	}

	if ia2.SignVal != eSignVal2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", eSignVal2, ia2.SignVal)
	}

	if ia2.Precision != ePrecision2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", ePrecision2, ia2.Precision)
	}

}

func TestIntAry_SetEqualArrayLengths_05(t *testing.T) {
	nStr1 := "-123456.143456"
	eNStr1 := "-123456.143456"
	ePrecision1 := 6
	eSignVal1 := -1

	nStr2 := "353678.123456"
	eNStr2 := "353678.123456"
	ePrecision2 := 6
	eSignVal2 := 1

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia1.SetEqualArrayLengths(&ia2)

	if ia1.GetNumStr() != eNStr1 {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' . Instead, ia1.GetNumStr()= '%v' .", eNStr1, ia1.GetNumStr())
	}

	if ia1.SignVal != eSignVal1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", eSignVal1, ia1.SignVal)
	}

	if ia1.Precision != ePrecision1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", ePrecision1, ia1.Precision)
	}

	if ia2.GetNumStr() != eNStr2 {
		t.Errorf("Error - Expected ia2.GetNumStr()= '%v' . Instead, ia2.GetNumStr()= '%v' .", eNStr2, ia2.GetNumStr())
	}

	if ia2.SignVal != eSignVal2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", eSignVal2, ia2.SignVal)
	}

	if ia2.Precision != ePrecision2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", ePrecision2, ia2.Precision)
	}

}

func TestIntAry_SetEqualArrayLengths_06(t *testing.T) {
	nStr1 := "0.00"
	eNStr1 := "0.00"
	ePrecision1 := 2
	eSignVal1 := 1

	nStr2 := "0"
	eNStr2 := "0.00"
	ePrecision2 := 2
	eSignVal2 := 1

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia1.SetEqualArrayLengths(&ia2)

	if ia1.GetNumStr() != eNStr1 {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' . Instead, ia1.GetNumStr()= '%v' .", eNStr1, ia1.GetNumStr())
	}

	if ia1.SignVal != eSignVal1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", eSignVal1, ia1.SignVal)
	}

	if ia1.Precision != ePrecision1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", ePrecision1, ia1.Precision)
	}

	if ia2.GetNumStr() != eNStr2 {
		t.Errorf("Error - Expected ia2.GetNumStr()= '%v' . Instead, ia2.GetNumStr()= '%v' .", eNStr2, ia2.GetNumStr())
	}

	if ia2.SignVal != eSignVal2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", eSignVal2, ia2.SignVal)
	}

	if ia2.Precision != ePrecision2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", ePrecision2, ia2.Precision)
	}

}

func TestIntAry_SetEqualArrayLengths_07(t *testing.T) {
	nStr1 := "0"
	eNStr1 := "0.00"
	ePrecision1 := 2
	eSignVal1 := 1

	nStr2 := "0.00"
	eNStr2 := "0.00"
	ePrecision2 := 2
	eSignVal2 := 1

	ia1 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(nStr2)
	ia1.SetEqualArrayLengths(&ia2)

	if ia1.GetNumStr() != eNStr1 {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' . Instead, ia1.GetNumStr()= '%v' .", eNStr1, ia1.GetNumStr())
	}

	if ia1.SignVal != eSignVal1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", eSignVal1, ia1.SignVal)
	}

	if ia1.Precision != ePrecision1 {
		t.Errorf("Error - Expected ia1.SignVal= '%v' . Instead, ia1.SignVal= '%v' .", ePrecision1, ia1.Precision)
	}

	if ia2.GetNumStr() != eNStr2 {
		t.Errorf("Error - Expected ia2.GetNumStr()= '%v' . Instead, ia2.GetNumStr()= '%v' .", eNStr2, ia2.GetNumStr())
	}

	if ia2.SignVal != eSignVal2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", eSignVal2, ia2.SignVal)
	}

	if ia2.Precision != ePrecision2 {
		t.Errorf("Error - Expected ia2.SignVal= '%v' . Instead, ia2.SignVal= '%v' .", ePrecision2, ia2.Precision)
	}

}

func TestIntAry_SetIntAryWithFloat_01(t *testing.T) {
	num := float32(475.895)
	expectedNumStr := "475.895"
	precision := 3
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat32(num, precision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat32(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloat_02(t *testing.T) {
	num := float32(475.895)
	expectedNumStr := "475.8950"
	precision := 4
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat32(num, precision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat32(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloat_03(t *testing.T) {
	num := float32(475.895600)
	expectedNumStr := "475.8956"
	precision := 4
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat32(num, -1)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat32(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloat_04(t *testing.T) {
	num := float32(-475.8956)
	expectedNumStr := "-475.8956"
	precision := 4
	signVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat32(num, -1)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat32(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloat64_01(t *testing.T) {
	num := float64(4756625.895)
	expectedNumStr := "4756625.895"
	precision := 3
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat64(num, precision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat64(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloat64_02(t *testing.T) {
	num := float64(4756625.895)
	expectedNumStr := "4756625.8950"
	precision := 4
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat64(num, precision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat64(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloat64_03(t *testing.T) {
	num := float64(475.895600)
	expectedNumStr := "475.8956"
	precision := 4
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat64(num, -1)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat64(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloat64_04(t *testing.T) {
	num := float64(-475.8956)
	expectedNumStr := "-475.8956"
	precision := 4
	signVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloat64(num, -1)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloat64(num, precision). num= %v  precision= %v  Error= %v", num, precision, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloatBig_01(t *testing.T) {
	num := big.NewFloat(float64(4756625.895))
	expectedNumStr := "4756625.895"
	precision := 3
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloatBig(num)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloatBig(num). num= %v Error= %v", num, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloatBig_02(t *testing.T) {
	num := big.NewFloat(float64(4756625.8950))
	expectedNumStr := "4756625.895"
	precision := 3
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloatBig(num)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloatBig(num). num= %v  Error= %v", num, err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloatBig_03(t *testing.T) {
	num := big.NewFloat(float64(475.895600))
	expectedNumStr := "475.8956"
	precision := 4
	signVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloatBig(num)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloatBig(num). num= %v  Error= %v", num.String(), err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithFloatBig_04(t *testing.T) {

	num :=  big.NewFloat(float64(-475.8956))
	expectedNumStr := "-475.8956"
	precision := 4
	signVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithFloatBig(num)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithFloatBig(num). num= %v Error= %v", num.String(), err)
	}

	if expectedNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' .  Instead, ia.GetNumStr()= '%v' .", expectedNumStr, ia.GetNumStr())
	}

	if precision != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' .  Instead, ia.Precision= '%v' .", precision, ia.Precision)
	}

	if signVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' .  Instead, ia.SignVal= '%v' .", signVal, ia.SignVal)
	}
}

func TestIntAry_SetIntAryWithBigInt_01(t *testing.T) {

	num := big.NewInt(int64(123456789))

	eNumStr := "123456.789"
	ePrecision := uint(3)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithBigInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithBigInt(num, ePrecision). num= %v  ePrecision= %v  eSignVal= %v", num.String(), ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithBigInt_02(t *testing.T) {

	num := big.NewInt(int64(-123456789))

	eNumStr := "-123456.789"
	ePrecision := uint(3)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithBigInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithBigInt(num, ePrecision). num= %v  ePrecision= %v  .", num.String(), ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithBigInt_03(t *testing.T) {

	num := big.NewInt(int64(0))

	eNumStr := "0.000"
	ePrecision := uint(3)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithBigInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithBigInt(num, ePrecision). num= %v  ePrecision= %v  .", num.String(), ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt_01(t *testing.T) {

	num := 123456789

	eNumStr := "123456.789"
	ePrecision := uint(3)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt(num, ePrecision). num= %v  ePrecision= %v .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt_02(t *testing.T) {

	num := -123456789

	eNumStr := "-12345.6789"
	ePrecision := uint(4)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt_03(t *testing.T) {

	num := 0

	eNumStr := "0.0000"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt_04(t *testing.T) {

	num := 32

	eNumStr := "0.0032"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt_05(t *testing.T) {

	num := -32

	eNumStr := "-32"
	ePrecision := uint(0)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt_06(t *testing.T) {

	num := 32

	eNumStr := "0.32"
	ePrecision := uint(2)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt32_01(t *testing.T) {

	num := int32(123456789)

	eNumStr := "123456.789"
	ePrecision := uint(3)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt32(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt(num, ePrecision). num= %v  ePrecision= %v .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt32_02(t *testing.T) {

	num := int32(-123456789)

	eNumStr := "-12345.6789"
	ePrecision := uint(4)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt32(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt32(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt32_03(t *testing.T) {

	num := int32(0)

	eNumStr := "0.0000"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt32(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt32(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt32_04(t *testing.T) {

	num := int32(32)

	eNumStr := "0.0032"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt32(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt32(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt32_05(t *testing.T) {

	num := int32(-32)

	eNumStr := "-32"
	ePrecision := uint(0)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt32(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt32(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt32_06(t *testing.T) {

	num := int32(32)

	eNumStr := "0.32"
	ePrecision := uint(2)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt32(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt32(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithUint64_01(t *testing.T) {

	num := uint64(123456789)

	eNumStr := "123456.789"
	ePrecision := uint(3)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithUint64(num, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithUint64(num, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", num, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithUint64_02(t *testing.T) {

	num := uint64(123456789)

	eNumStr := "-12345.6789"
	ePrecision := uint(4)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithUint64(num, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithUint64(num, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", num, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithUint64_03(t *testing.T) {

	num := uint64(0)

	eNumStr := "0.0000"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithUint64(num, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithUint64(num, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", num, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithUint64_04(t *testing.T) {

	num := uint64(32)

	eNumStr := "0.0032"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithUint64(num, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithUint64(num, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", num, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithUint64_05(t *testing.T) {

	num := uint64(32)

	eNumStr := "-32"
	ePrecision := uint(0)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithUint64(num, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithUint64(num, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", num, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithUint64_06(t *testing.T) {

	num := uint64(32)

	eNumStr := "0.32"
	ePrecision := uint(2)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithUint64(num, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithUint64(num, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", num, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt64_01(t *testing.T) {

	num := int64(123456789)

	eNumStr := "123456.789"
	ePrecision := uint(3)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt64(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt64(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt64_02(t *testing.T) {

	num := int64(-123456789)

	eNumStr := "-12345.6789"
	ePrecision := uint(4)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt64(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt64(num, ePrecision). num= %v  ePrecision= %v .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt64_03(t *testing.T) {

	num := int64(0)

	eNumStr := "0.0000"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt64(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt64(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt64_04(t *testing.T) {

	num := int64(32)

	eNumStr := "0.0032"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt64(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt64(num, ePrecision, eSignVal). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt64_05(t *testing.T) {

	num := int64(-32)

	eNumStr := "-32"
	ePrecision := uint(0)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt64(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt64(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithInt64_06(t *testing.T) {

	num := int64(32)

	eNumStr := "0.32"
	ePrecision := uint(2)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithInt64(num, ePrecision)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithInt64(num, ePrecision). num= %v  ePrecision= %v  .", num, ePrecision)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithIntAry_01(t *testing.T) {
	iAry := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	eNumStr := "123456.789"
	ePrecision := uint(3)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", iAry, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithIntAry_02(t *testing.T) {
	iAry := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	eNumStr := "-12345.6789"
	ePrecision := uint(4)
	eSignVal := -1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", iAry, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithIntAry_03(t *testing.T) {
	iAry := []int{3, 2}
	eNumStr := "0.0032"
	ePrecision := uint(4)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", iAry, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithIntAry_04(t *testing.T) {
	iAry := []int{3, 2}
	eNumStr := "0.32"
	ePrecision := uint(2)
	eSignVal := 1

	ia := IntAry{}.New()

	err := ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal)

	if err != nil {
		t.Errorf("Error returned from ia.SetIntAryWithIntAry(iAry, ePrecision, eSignVal). num= %v  ePrecision= %v  eSignVal= %v", iAry, ePrecision, eSignVal)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetIntAryWithIntAryObj_01(t *testing.T) {

	numStr1 := "0000589432.607528000"
	numStr2 := "-028.3700"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(numStr1)

	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(numStr2)

	err := ia2.SetIntAryWithIntAryObj(&ia, false)

	if err != nil {
		t.Errorf("Received Error from ia2.SetIntAryWithIntAryObj(&ia, false). Error= %v", err)
	}

	if !ia.Equals(&ia2) {
		t.Error("ia is NOT EQUAL to ia2!!!")
	}

}

func TestIntAry_SetIntAryWithIntAryObj_02(t *testing.T) {

	numStr1 := "0000589432.607528000"
	numStr2 := "-028.3700"

	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(numStr1)

	ia2 := IntAry{}.New()
	ia2.SetIntAryWithNumStr(numStr2)

	err := ia2.SetIntAryWithIntAryObj(&ia, true)

	if err != nil {
		t.Errorf("Received Error from ia2.SetIntAryWithIntAryObj(&ia, true). Error= %v", err)
	}

	if !ia.Equals(&ia2) {
		t.Error("ia is NOT EQUAL to ia2 !!!")
	}

	if !ia.BackUp.Equals(&ia2.BackUp) {
		t.Error("ia.BackUp is NOT EQUAL to ia2.BackUp !!!")
	}

}

func TestIntAry_SetWithNumStr_01(t *testing.T) {
	nStr := "123.456"
	nRunes := []rune("123456")
	eIAry := []int{1, 2, 3, 4, 5, 6}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.SetIntAryLength()

	if ia.GetNumStr() != nStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", nStr, ia.GetNumStr())
	}

	if ia.Precision != 3 {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", 3, ia.Precision)
	}

	if ia.SignVal != 1 {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", 1, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_SetWithNumStr_02(t *testing.T) {
	nStr := "-12345.9"
	nRunes := []rune("123459")
	eIAry := []int{1, 2, 3, 4, 5, 9}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 1
	eSignVal := -1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.SetIntAryLength()

	if ia.GetNumStr() != nStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", nStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_SetWithNumStr_03(t *testing.T) {
	nStrRaw := "-123  45.9"
	nStr := "-12345.9"
	nRunes := []rune("123459")
	eIAry := []int{1, 2, 3, 4, 5, 9}
	lNRunes := len(nRunes)
	lEArray := len(eIAry)
	ePrecision := 1
	eSignVal := -1

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStrRaw)

	if err != nil {
		t.Errorf("Received Error from ia.SetIntAryWithNumStr(nStr). nStr= '%v' Error= %v", nStr, err)
	}

	ia.SetIntAryLength()

	if ia.GetNumStr() != nStr {
		t.Errorf("Error: Expected numStr= '%v'. Instead received numStr= '%v'", nStr, ia.GetNumStr())
	}

	if ia.Precision != ePrecision {
		t.Errorf("Error: Expected Precision= '%v'. Instead received Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Error: Expected SignVal= '%v'. Instead received SignVal= '%v'", eSignVal, ia.SignVal)
	}

	if lNRunes != ia.NumRunesLen {
		t.Errorf("Error: Expected NumRunes Length= '%v'. Instead received NumRunes Length= '%v'", lNRunes, ia.NumRunesLen)
	}

	if lEArray != ia.IntAryLen {
		t.Errorf("Error: Expected IntArray Length= '%v'. Instead received IntArry Length= '%v'", lEArray, ia.IntAryLen)
	}

	for i := 0; i < lNRunes; i++ {

		if nRunes[i] != ia.NumRunes[i] {
			t.Error("Error: Expected nRunes Array does NOT match ia.NumRunes Array! ")
			return
		}

	}

	for i := 0; i < lEArray; i++ {
		if eIAry[i] != ia.IntAry[i] {

			t.Error("Error: Expected IntAry Array does NOT match ia.IntAry! ")
			return

		}
	}

}

func TestIntAry_SetPrecision_01(t *testing.T) {
	nStr1 := "99.995"
	expected := "99.99"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.SetPrecision(precision, false)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_SetPrecision_02(t *testing.T) {
	nStr1 := "99.995"
	expected := "100.00"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.SetPrecision(precision, true)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_SetPrecision_03(t *testing.T) {
	nStr1 := "-0"
	expected := "0.00"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.SetPrecision(precision, true)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_SetPrecision_04(t *testing.T) {
	nStr1 := "-999.995"
	expected := "-1000.00"
	precision := 2
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.SetPrecision(precision, true)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_SetPrecision_05(t *testing.T) {
	nStr1 := "-999.995"
	expected := "-999.995"
	precision := 3
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.SetPrecision(precision, true)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_SetPrecision_06(t *testing.T) {
	nStr1 := "-999.995"
	expected := "-999.995000"
	precision := 6
	ia := IntAry{}.New()
	ia.SetIntAryWithNumStr(nStr1)
	err := ia.SetPrecision(precision, true)
	if err != nil {
		t.Errorf("Received Error from ia.RoundToPrecision(precision). precision '%v' Error:= %v", precision, err)
	}

	s := ia.GetNumStr()
	if expected != s {
		t.Errorf("Error. Expected numStr= '%v'. Instead, got numStr='%v'\n", expected, s)
	}

	if ia.Precision != precision {
		t.Errorf("Error. Expected precision= '%v'. Instead, got precision='%v'\n", precision, ia.Precision)
	}

}

func TestIntAry_SetSignificantDigitIdxs_01(t *testing.T) {
	nStr := ".7770"
	eAryLen := 5
	eNumStr := "0.7770"
	eIntegerLen := 1
	eSigIntegerLen := 1
	eSigFractionLen := 3
	eIsZeroValue := false
	eIsIntegerZeroValue := true
	eFirstDigitIdx := 0
	eLastDigitIdx := 3
	eSignVal := 1
	ePrecision := 4

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Error returned from SetIntAryWithNumStr(nStr). numStr= %v ", eNumStr)
	}

	if eFirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Expected ia.FirstDigitIdx= '%v' .  Instead, ia.FirstDigitIdx= '%v' .", eFirstDigitIdx, ia.FirstDigitIdx)
	}

	if eLastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Expected ia.LastDigitIdx= '%v' .  Instead, ia.LastDigitIdx= '%v' .", eLastDigitIdx, ia.LastDigitIdx)
	}

	eIsZeroValue = !eIsZeroValue
	eIsZeroValue = !eIsZeroValue

	if eIsZeroValue != ia.IsZeroValue {
		t.Errorf("Expected ia.IsZeroValue= '%v' .  Instead, ia.IsZeroValue= '%v' .", eIsZeroValue, ia.IsZeroValue)
	}

	eIsIntegerZeroValue = !eIsIntegerZeroValue
	eIsIntegerZeroValue = !eIsIntegerZeroValue

	if eIsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Expected ia.IsIntegerZeroValue= '%v' .  Instead, ia.IsIntegerZeroValue= '%v' .", eIsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if eAryLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .  Instead, ia.IntAryLen= '%v' .", eAryLen, ia.IntAryLen)
	}

	if eIntegerLen != ia.IntegerLen {
		t.Errorf("Expected ia.IntegerLen= '%v' .  Instead, ia.IntegerLen= '%v' .", eIntegerLen, ia.IntegerLen)
	}

	if eSigFractionLen != ia.SignificantFractionLen {
		t.Errorf("Expected ia.SignificantFractionLen= '%v' .  Instead, ia.SignificantFractionLen= '%v' .", eSigFractionLen, ia.SignificantFractionLen)
	}

	if eSigIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Expected ia.SignificantIntegerLen= '%v' .  Instead, ia.SignificantIntegerLen= '%v' .", eSigIntegerLen, ia.SignificantIntegerLen)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetSignificantDigitIdxs_02(t *testing.T) {
	nStr := "000123456.123456000"
	eAryLen := 18
	eNumStr := "000123456.123456000"
	eIntegerLen := 9
	eSigIntegerLen := 6
	eSigFractionLen := 6
	eIsZeroValue := false
	eIsIntegerZeroValue := false
	eFirstDigitIdx := 3
	eLastDigitIdx := 14
	eSignVal := 1
	ePrecision := 9

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Error returned from SetIntAryWithNumStr(nStr). numStr= %v ", eNumStr)
	}

	if eFirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Expected ia.FirstDigitIdx= '%v' .  Instead, ia.FirstDigitIdx= '%v' .", eFirstDigitIdx, ia.FirstDigitIdx)
	}

	if eLastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Expected ia.LastDigitIdx= '%v' .  Instead, ia.LastDigitIdx= '%v' .", eLastDigitIdx, ia.LastDigitIdx)
	}

	eIsZeroValue = !eIsZeroValue
	eIsZeroValue = !eIsZeroValue

	if eIsZeroValue != ia.IsZeroValue {
		t.Errorf("Expected ia.IsZeroValue= '%v' .  Instead, ia.IsZeroValue= '%v' .", eIsZeroValue, ia.IsZeroValue)
	}

	eIsIntegerZeroValue = !eIsIntegerZeroValue
	eIsIntegerZeroValue = !eIsIntegerZeroValue

	if eIsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Expected ia.IsIntegerZeroValue= '%v' .  Instead, ia.IsIntegerZeroValue= '%v' .", eIsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if eAryLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .  Instead, ia.IntAryLen= '%v' .", eAryLen, ia.IntAryLen)
	}

	if eIntegerLen != ia.IntegerLen {
		t.Errorf("Expected ia.IntegerLen= '%v' .  Instead, ia.IntegerLen= '%v' .", eIntegerLen, ia.IntegerLen)
	}

	if eSigFractionLen != ia.SignificantFractionLen {
		t.Errorf("Expected ia.SignificantFractionLen= '%v' .  Instead, ia.SignificantFractionLen= '%v' .", eSigFractionLen, ia.SignificantFractionLen)
	}

	if eSigIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Expected ia.SignificantIntegerLen= '%v' .  Instead, ia.SignificantIntegerLen= '%v' .", eSigIntegerLen, ia.SignificantIntegerLen)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetSignificantDigitIdxs_03(t *testing.T) {
	nStr := "-000123456.123456000"
	eAryLen := 18
	eNumStr := "-000123456.123456000"
	eIntegerLen := 9
	eSigIntegerLen := 6
	eSigFractionLen := 6
	eIsZeroValue := false
	eIsIntegerZeroValue := false
	eFirstDigitIdx := 3
	eLastDigitIdx := 14
	eSignVal := -1
	ePrecision := 9

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Error returned from SetIntAryWithNumStr(nStr). numStr= %v ", eNumStr)
	}

	if eFirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Expected ia.FirstDigitIdx= '%v' .  Instead, ia.FirstDigitIdx= '%v' .", eFirstDigitIdx, ia.FirstDigitIdx)
	}

	if eLastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Expected ia.LastDigitIdx= '%v' .  Instead, ia.LastDigitIdx= '%v' .", eLastDigitIdx, ia.LastDigitIdx)
	}

	eIsZeroValue = !eIsZeroValue
	eIsZeroValue = !eIsZeroValue

	if eIsZeroValue != ia.IsZeroValue {
		t.Errorf("Expected ia.IsZeroValue= '%v' .  Instead, ia.IsZeroValue= '%v' .", eIsZeroValue, ia.IsZeroValue)
	}

	eIsIntegerZeroValue = !eIsIntegerZeroValue
	eIsIntegerZeroValue = !eIsIntegerZeroValue

	if eIsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Expected ia.IsIntegerZeroValue= '%v' .  Instead, ia.IsIntegerZeroValue= '%v' .", eIsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if eAryLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .  Instead, ia.IntAryLen= '%v' .", eAryLen, ia.IntAryLen)
	}

	if eIntegerLen != ia.IntegerLen {
		t.Errorf("Expected ia.IntegerLen= '%v' .  Instead, ia.IntegerLen= '%v' .", eIntegerLen, ia.IntegerLen)
	}

	if eSigFractionLen != ia.SignificantFractionLen {
		t.Errorf("Expected ia.SignificantFractionLen= '%v' .  Instead, ia.SignificantFractionLen= '%v' .", eSigFractionLen, ia.SignificantFractionLen)
	}

	if eSigIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Expected ia.SignificantIntegerLen= '%v' .  Instead, ia.SignificantIntegerLen= '%v' .", eSigIntegerLen, ia.SignificantIntegerLen)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetSignificantDigitIdxs_04(t *testing.T) {
	nStr := "000.123456000"
	eAryLen := 12
	eNumStr := "000.123456000"
	eIntegerLen := 3
	eSigIntegerLen := 1
	eSigFractionLen := 6
	eIsZeroValue := false
	eIsIntegerZeroValue := true
	eFirstDigitIdx := 2
	eLastDigitIdx := 8
	eSignVal := 1
	ePrecision := 9

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Error returned from SetIntAryWithNumStr(nStr). numStr= %v ", eNumStr)
	}

	if eFirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Expected ia.FirstDigitIdx= '%v' .  Instead, ia.FirstDigitIdx= '%v' .", eFirstDigitIdx, ia.FirstDigitIdx)
	}

	if eLastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Expected ia.LastDigitIdx= '%v' .  Instead, ia.LastDigitIdx= '%v' .", eLastDigitIdx, ia.LastDigitIdx)
	}

	eIsZeroValue = !eIsZeroValue
	eIsZeroValue = !eIsZeroValue

	if eIsZeroValue != ia.IsZeroValue {
		t.Errorf("Expected ia.IsZeroValue= '%v' .  Instead, ia.IsZeroValue= '%v' .", eIsZeroValue, ia.IsZeroValue)
	}

	eIsIntegerZeroValue = !eIsIntegerZeroValue
	eIsIntegerZeroValue = !eIsIntegerZeroValue

	if eIsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Expected ia.IsIntegerZeroValue= '%v' .  Instead, ia.IsIntegerZeroValue= '%v' .", eIsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if eAryLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .  Instead, ia.IntAryLen= '%v' .", eAryLen, ia.IntAryLen)
	}

	if eIntegerLen != ia.IntegerLen {
		t.Errorf("Expected ia.IntegerLen= '%v' .  Instead, ia.IntegerLen= '%v' .", eIntegerLen, ia.IntegerLen)
	}

	if eSigFractionLen != ia.SignificantFractionLen {
		t.Errorf("Expected ia.SignificantFractionLen= '%v' .  Instead, ia.SignificantFractionLen= '%v' .", eSigFractionLen, ia.SignificantFractionLen)
	}

	if eSigIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Expected ia.SignificantIntegerLen= '%v' .  Instead, ia.SignificantIntegerLen= '%v' .", eSigIntegerLen, ia.SignificantIntegerLen)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetSignificantDigitIdxs_05(t *testing.T) {
	nStr := "256"
	eAryLen := 3
	eNumStr := "256"
	eIntegerLen := 3
	eSigIntegerLen := 3
	eSigFractionLen := 0
	eIsZeroValue := false
	eIsIntegerZeroValue := false
	eFirstDigitIdx := 0
	eLastDigitIdx := 2
	eSignVal := 1
	ePrecision := 0

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Error returned from SetIntAryWithNumStr(nStr). numStr= %v ", eNumStr)
	}

	if eFirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Expected ia.FirstDigitIdx= '%v' .  Instead, ia.FirstDigitIdx= '%v' .", eFirstDigitIdx, ia.FirstDigitIdx)
	}

	if eLastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Expected ia.LastDigitIdx= '%v' .  Instead, ia.LastDigitIdx= '%v' .", eLastDigitIdx, ia.LastDigitIdx)
	}

	eIsZeroValue = !eIsZeroValue
	eIsZeroValue = !eIsZeroValue

	if eIsZeroValue != ia.IsZeroValue {
		t.Errorf("Expected ia.IsZeroValue= '%v' .  Instead, ia.IsZeroValue= '%v' .", eIsZeroValue, ia.IsZeroValue)
	}

	eIsIntegerZeroValue = !eIsIntegerZeroValue
	eIsIntegerZeroValue = !eIsIntegerZeroValue

	if eIsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Expected ia.IsIntegerZeroValue= '%v' .  Instead, ia.IsIntegerZeroValue= '%v' .", eIsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if eAryLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .  Instead, ia.IntAryLen= '%v' .", eAryLen, ia.IntAryLen)
	}

	if eIntegerLen != ia.IntegerLen {
		t.Errorf("Expected ia.IntegerLen= '%v' .  Instead, ia.IntegerLen= '%v' .", eIntegerLen, ia.IntegerLen)
	}

	if eSigFractionLen != ia.SignificantFractionLen {
		t.Errorf("Expected ia.SignificantFractionLen= '%v' .  Instead, ia.SignificantFractionLen= '%v' .", eSigFractionLen, ia.SignificantFractionLen)
	}

	if eSigIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Expected ia.SignificantIntegerLen= '%v' .  Instead, ia.SignificantIntegerLen= '%v' .", eSigIntegerLen, ia.SignificantIntegerLen)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

func TestIntAry_SetSignificantDigitIdxs_06(t *testing.T) {
	nStr := "000256"
	eAryLen := 6
	eNumStr := "000256"
	eIntegerLen := 6
	eSigIntegerLen := 3
	eSigFractionLen := 0
	eIsZeroValue := false
	eIsIntegerZeroValue := false
	eFirstDigitIdx := 3
	eLastDigitIdx := 5
	eSignVal := 1
	ePrecision := 0

	ia := IntAry{}.New()
	err := ia.SetIntAryWithNumStr(nStr)

	if err != nil {
		t.Errorf("Error returned from SetIntAryWithNumStr(nStr). numStr= %v ", eNumStr)
	}

	if eFirstDigitIdx != ia.FirstDigitIdx {
		t.Errorf("Expected ia.FirstDigitIdx= '%v' .  Instead, ia.FirstDigitIdx= '%v' .", eFirstDigitIdx, ia.FirstDigitIdx)
	}

	if eLastDigitIdx != ia.LastDigitIdx {
		t.Errorf("Expected ia.LastDigitIdx= '%v' .  Instead, ia.LastDigitIdx= '%v' .", eLastDigitIdx, ia.LastDigitIdx)
	}

	eIsZeroValue = !eIsZeroValue
	eIsZeroValue = !eIsZeroValue

	if eIsZeroValue != ia.IsZeroValue {
		t.Errorf("Expected ia.IsZeroValue= '%v' .  Instead, ia.IsZeroValue= '%v' .", eIsZeroValue, ia.IsZeroValue)
	}

	eIsIntegerZeroValue = !eIsIntegerZeroValue
	eIsIntegerZeroValue = !eIsIntegerZeroValue

	if eIsIntegerZeroValue != ia.IsIntegerZeroValue {
		t.Errorf("Expected ia.IsIntegerZeroValue= '%v' .  Instead, ia.IsIntegerZeroValue= '%v' .", eIsIntegerZeroValue, ia.IsIntegerZeroValue)
	}

	if eAryLen != ia.IntAryLen {
		t.Errorf("Expected ia.IntAryLen= '%v' .  Instead, ia.IntAryLen= '%v' .", eAryLen, ia.IntAryLen)
	}

	if eIntegerLen != ia.IntegerLen {
		t.Errorf("Expected ia.IntegerLen= '%v' .  Instead, ia.IntegerLen= '%v' .", eIntegerLen, ia.IntegerLen)
	}

	if eSigFractionLen != ia.SignificantFractionLen {
		t.Errorf("Expected ia.SignificantFractionLen= '%v' .  Instead, ia.SignificantFractionLen= '%v' .", eSigFractionLen, ia.SignificantFractionLen)
	}

	if eSigIntegerLen != ia.SignificantIntegerLen {
		t.Errorf("Expected ia.SignificantIntegerLen= '%v' .  Instead, ia.SignificantIntegerLen= '%v' .", eSigIntegerLen, ia.SignificantIntegerLen)
	}

	if eNumStr != ia.GetNumStr() {
		t.Errorf("Expected ia.GetNumStr()= '%v' . Instead, ia.GetNumStr()= '%v'", eNumStr, ia.GetNumStr())
	}

	if int(ePrecision) != ia.Precision {
		t.Errorf("Expected ia.Precision= '%v' . Instead, ia.Precision= '%v'", ePrecision, ia.Precision)
	}

	if eSignVal != ia.SignVal {
		t.Errorf("Expected ia.SignVal= '%v' . Instead, ia.SignVal= '%v'", eSignVal, ia.SignVal)
	}

}

