package common

import (

	"testing"
)



func TestIntAry_SubtractFromThis_01(t *testing.T) {
	nStr1 := "900.777"
	nStr2 := "901.000"
	eNumStr := "-0.223"
	ePrecision := 3
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_02(t *testing.T) {
	nStr1 := "350"
	nStr2 := "122"
	eNumStr := "228"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_03(t *testing.T) {
	nStr1 := "-350"
	nStr2 := "122"
	eNumStr := "-472"
	ePrecision := 0
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_04(t *testing.T) {
	nStr1 := "-350"
	nStr2 := "-122"
	eNumStr := "-228"
	ePrecision := 0
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_05(t *testing.T) {
	nStr1 := "350"
	nStr2 := "-122"
	eNumStr := "472"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_06(t *testing.T) {
	nStr1 := "350"
	nStr2 := "0"
	eNumStr := "350"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_07(t *testing.T) {
	nStr1 := "-350"
	nStr2 := "0"
	eNumStr := "-350"
	ePrecision := 0
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_08(t *testing.T) {
	nStr1 := "122"
	nStr2 := "350"
	eNumStr := "-228"
	ePrecision := 0
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_09(t *testing.T) {
	nStr1 := "-122"
	nStr2 := "350"
	eNumStr := "-472"
	ePrecision := 0
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_10(t *testing.T) {
	nStr1 := "-122"
	nStr2 := "-350"
	eNumStr := "228"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_11(t *testing.T) {
	nStr1 := "122"
	nStr2 := "-350"
	eNumStr := "472"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_12(t *testing.T) {
	nStr1 := "0"
	nStr2 := "350"
	eNumStr := "-350"
	ePrecision := 0
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_13(t *testing.T) {
	nStr1 := "0"
	nStr2 := "-350"
	eNumStr := "350"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_14(t *testing.T) {
	nStr1 := "122"
	nStr2 := "122"
	eNumStr := "0"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_15(t *testing.T) {
	nStr1 := "-122"
	nStr2 := "122"
	eNumStr := "-244"
	ePrecision := 0
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_16(t *testing.T) {
	nStr1 := "-122"
	nStr2 := "-122"
	eNumStr := "0"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_17(t *testing.T) {
	nStr1 := "122"
	nStr2 := "-122"
	eNumStr := "244"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_18(t *testing.T) {
	nStr1 := "0"
	nStr2 := "0"
	eNumStr := "0"
	ePrecision := 0
	eSignVal := 1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}

func TestIntAry_SubtractFromThis_19(t *testing.T) {
	nStr1 := "1.122"
	nStr2 := "4.5"
	eNumStr := "-3.378"
	ePrecision := 3
	eSignVal := -1

	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	err := ia1.SubtractFromThis(&ia2, true)

	if err != nil {
		t.Errorf("Error returned from ia1.SubtractFromThis(&ia2, true). Error= %v", err)
	}

	if eNumStr != ia1.GetNumStr() {
		t.Errorf("Error - Expected ia1.GetNumStr()= '%v' .  Instead, ia1.GetNumStr()= '%v' .", eNumStr, ia1.GetNumStr())
	}

	if ePrecision != ia1.Precision {
		t.Errorf("Error - Expected ia1.Precision= '%v' .  Instead, ia1.Precision= '%v' .", ePrecision, ia1.Precision)
	}

	if eSignVal != ia1.SignVal {
		t.Errorf("Error - Expected ia1.SignVal= '%v' .  Instead, ia1.SignVal= '%v' .", eSignVal, ia1.SignVal)
	}

}


func TestIntAry_SubtractMultipleFromThis_01(t *testing.T) {
	nStrBase := "197.452"
	nStr1 := "1.122"
	nStr2 := "4.5"
	nStr3 := "32.148"
	nStr4 := "10.0"
	eNumStr := "149.682"
	ePrecision := 3
	eSignVal := 1

	iaBase := IntAry{}.New()
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia3 := IntAry{}.New()
	ia4 := IntAry{}.New()
	iaBase.SetIntAryWithNumStr(nStrBase)
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	ia3.SetIntAryWithNumStr(nStr3)
	ia4.SetIntAryWithNumStr(nStr4)
	err := iaBase.SubtractMultipleFromThis(true, &ia1, &ia2, &ia3, &ia4)

	if err != nil {
		t.Errorf("Error returned from iaBase.SubtractMultipleFromThis(true, ia...). Error= %v", err)
	}

	if eNumStr != iaBase.GetNumStr() {
		t.Errorf("Error - Expected iaBase.GetNumStr()= '%v' .  Instead, iaBase.GetNumStr()= '%v' .", eNumStr, iaBase.GetNumStr())
	}

	if ePrecision != iaBase.Precision {
		t.Errorf("Error - Expected iaBase.Precision= '%v' .  Instead, iaBase.Precision= '%v' .", ePrecision, iaBase.Precision)
	}

	if eSignVal != iaBase.SignVal {
		t.Errorf("Error - Expected iaBase.SignVal= '%v' .  Instead, iaBase.SignVal= '%v' .", eSignVal, iaBase.SignVal)
	}

}

func TestIntAry_SubtractMultipleFromThis_02(t *testing.T) {
	nStrBase := "197.452"
	nStr1 := "1.122"
	nStr2 := "4.5"
	nStr3 := "-32.148"
	nStr4 := "10.0"
	eNumStr := "213.978"
	ePrecision := 3
	eSignVal := 1

	iaBase := IntAry{}.New()
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia3 := IntAry{}.New()
	ia4 := IntAry{}.New()
	iaBase.SetIntAryWithNumStr(nStrBase)
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	ia3.SetIntAryWithNumStr(nStr3)
	ia4.SetIntAryWithNumStr(nStr4)
	err := iaBase.SubtractMultipleFromThis(true, &ia1, &ia2, &ia3, &ia4)

	if err != nil {
		t.Errorf("Error returned from iaBase.SubtractMultipleFromThis(true, ia...). Error= %v", err)
	}

	if eNumStr != iaBase.GetNumStr() {
		t.Errorf("Error - Expected iaBase.GetNumStr()= '%v' .  Instead, iaBase.GetNumStr()= '%v' .", eNumStr, iaBase.GetNumStr())
	}

	if ePrecision != iaBase.Precision {
		t.Errorf("Error - Expected iaBase.Precision= '%v' .  Instead, iaBase.Precision= '%v' .", ePrecision, iaBase.Precision)
	}

	if eSignVal != iaBase.SignVal {
		t.Errorf("Error - Expected iaBase.SignVal= '%v' .  Instead, iaBase.SignVal= '%v' .", eSignVal, iaBase.SignVal)
	}

}

func TestIntAry_SubtractMultipleFromThis_03(t *testing.T) {
	nStrBase := "197.452"
	nStr1 := "1.122"
	nStr2 := "4.5"
	nStr3 := "932.148"
	nStr4 := "10.0"
	eNumStr := "-750.318"
	ePrecision := 3
	eSignVal := -1

	iaBase := IntAry{}.New()
	ia1 := IntAry{}.New()
	ia2 := IntAry{}.New()
	ia3 := IntAry{}.New()
	ia4 := IntAry{}.New()
	iaBase.SetIntAryWithNumStr(nStrBase)
	ia1.SetIntAryWithNumStr(nStr1)
	ia2.SetIntAryWithNumStr(nStr2)
	ia3.SetIntAryWithNumStr(nStr3)
	ia4.SetIntAryWithNumStr(nStr4)
	err := iaBase.SubtractMultipleFromThis(true, &ia1, &ia2, &ia3, &ia4)

	if err != nil {
		t.Errorf("Error returned from iaBase.SubtractMultipleFromThis(true, ia...). Error= %v", err)
	}

	if eNumStr != iaBase.GetNumStr() {
		t.Errorf("Error - Expected iaBase.GetNumStr()= '%v' .  Instead, iaBase.GetNumStr()= '%v' .", eNumStr, iaBase.GetNumStr())
	}

	if ePrecision != iaBase.Precision {
		t.Errorf("Error - Expected iaBase.Precision= '%v' .  Instead, iaBase.Precision= '%v' .", ePrecision, iaBase.Precision)
	}

	if eSignVal != iaBase.SignVal {
		t.Errorf("Error - Expected iaBase.SignVal= '%v' .  Instead, iaBase.SignVal= '%v' .", eSignVal, iaBase.SignVal)
	}

}
