# stringmathops

Contains data structures used in performing string and integer math
operations.

The primary structure and associated methods are contained in the source
code file, 'intary.go' located in the 'common' directory.

Data structures and methods of integer array management and math
operations are contained in the source code file 'intary.go'.
The primary data structure is 'IntAry'. The structure 'IntAry' and
the associated methods necessary to perform a variety of math operations
on integer arrays and number strings.

'strmathop.go' contains methods which perform math operations using
multiple 'IntAry' objects.

Source code for 'intary.go' and 'strmathop.go' is maintained in repository:

 https://bitbucket.org/AmarilloMike/stringmathops/src

### Dependencies
IntAry located in source code file, 'intary.go'. It is dependent on
structure 'NthRootOp' located in source code file, 'nthroot.go'

'nthroot.go' is maintained in respository:

 https://bitbucket.org/AmarilloMike/mathhlpr/src
